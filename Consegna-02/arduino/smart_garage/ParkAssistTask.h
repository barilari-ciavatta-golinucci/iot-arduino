#ifndef __PARKASSISTTASK__
#define __PARKASSISTTASK__

#include "Task.h"
#include "Context.h"
#include "Log.h"
#include "Arduino.h"

class ParkAssistTask: public Task {
    enum State {WAITING_CAR, PARK_ASSIST, PARKING_OK};

    public:
        ParkAssistTask();
        void init(int period);
        void tick();

    private:        
        Context* context;
        State state;
        unsigned int lastDistance;
        void computeLedPark(unsigned int distance);
        bool waitingCarEntryFlag;
        bool parkingOkEntryFlag;
};

#endif