#ifndef __VIRTUAL_PROX__
#define __VIRTUAL_PROX__

#include "Prox.h"

#define INITIAL_DISTANCE 100
#define MAX_DECREMENT_VALUE 3
#define DECREMENT_CASE 5

class VirtualProx: public Prox {
    public:
        VirtualProx();        
        int getDistance();
        void reset();
    private:
        int distance;
};

#endif
