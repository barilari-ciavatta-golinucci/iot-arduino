#include "VirtualPir.h"

VirtualPir::VirtualPir() {
    randomSeed(analogRead(A5));
    this->reset();
}

bool VirtualPir::getPresence() {
    if (this->counter++ == this->threshold) {
        return true;
    }
    return false;
}

void VirtualPir::reset() {
    this->threshold = random(MIN_THRESHOLD, MAX_THRESHOLD);
    this->counter = 0;
}
