#ifndef __VIRTUAL_PIR__
#define __VIRTUAL_PIR__

#include "Pir.h"
#define MIN_THRESHOLD 20
#define MAX_THRESHOLD 40

class VirtualPir: public Pir {
    public:
        VirtualPir();
        bool getPresence();
        void reset();
    private:
        int threshold;
        int counter;
};

#endif
