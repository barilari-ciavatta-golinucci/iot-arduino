#include "PhysicalPir.h"

PhysicalPir::PhysicalPir(int pin) {
    this->pin = pin;
    pinMode(this->pin, INPUT);
}

bool PhysicalPir::getPresence() {
    return digitalRead(pin) == HIGH;
}

void PhysicalPir::reset() {}
