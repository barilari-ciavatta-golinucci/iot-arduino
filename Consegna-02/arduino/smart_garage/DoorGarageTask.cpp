#include "DoorGarageTask.h"

DoorGarageTask::DoorGarageTask() {
    context = Context::getInstance();
}

void DoorGarageTask::init(int period) {
    Task::init(period);    
    periodTime = period;
    state = CLOSED_DOOR;
    elapsedTime = 0;
    closingDoorEntryFlag = false;
    openDoorEntryFlag = false;
}

void DoorGarageTask::tick() {
    String serialCommand = "";
    
    switch (state) {
        case CLOSED_DOOR:            
            while (Serial.available()) {
                serialCommand += (char) Serial.read();
            }

            if (serialCommand == "OPEN\n") {
                state = OPENING_DOOR;
                Log::info("DoorGarageTask: CLOSED_DOOR -> OPENING_DOOR");
                elapsedTime = 0;
            }

            break;
        case OPENING_DOOR:            
            context->ledLR->setIntensity(elapsedTime / (double) LED_FADE_DURATION);
            elapsedTime += periodTime;

            if (elapsedTime >= LED_FADE_DURATION) {
                elapsedTime = 0;
                state = WAITING_CAR;
                Log::info("DoorGarageTask: OPENING_DOOR -> WAITING_CAR");
                context->ledLR->switchOn();
            }

            break;
        case WAITING_CAR:
            elapsedTime += periodTime;

            if (context->pir->getPresence()) {
                elapsedTime = 0;
                state = OPEN_DOOR;
                Log::info("DoorGarageTask: WAITING_CAR -> OPEN_DOOR");
            } else if (elapsedTime >= N) {
                Serial.println("NOBODY");
                elapsedTime = 0;
                state = CLOSING_DOOR;
                Log::info("DoorGarageTask: WAITING_CAR -> CLOSING_DOOR");
            }

            break;
        case CLOSING_DOOR:
            if (!closingDoorEntryFlag) {                
                context->ledLDIST1->switchOff();
                context->ledLDIST2->switchOff();
                context->closeDoorMsg = false;
                closingDoorEntryFlag = true;
            }

            context->ledLR->setIntensity(1 - (elapsedTime / (double) LED_FADE_DURATION));
            elapsedTime += periodTime;            

            if (elapsedTime >= LED_FADE_DURATION) {
                elapsedTime = 0;
                state = CLOSED_DOOR;
                Log::info("DoorGarageTask: CLOSING_DOOR -> CLOSED_DOOR");
                context->ledLR->switchOff();
                context->pir->reset();
                context->prox->reset();
                closingDoorEntryFlag = false;
            }

            break;
        case OPEN_DOOR:
            if (!openDoorEntryFlag) {
                Serial.println("WELCOME_HOME");
                context->startProxMsg = true;
                openDoorEntryFlag = true;
            }
            
            if (context->closeDoorMsg || (context->closeButton->isPressed() && context->prox->getDistance() <= DIST_CLOSE)) {
                Serial.println("OK");
                state = CLOSING_DOOR;
                Log::info("DoorGarageTask: OPEN_DOOR -> CLOSING_DOOR");
                openDoorEntryFlag = false;
                context->startProxMsg = false;
            }

            break;            
    }
}
