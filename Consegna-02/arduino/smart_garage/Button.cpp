#include "Button.h"

Button::Button(unsigned int pin) {
    this->pin = pin;
    pinMode(pin, INPUT);     
} 
  
bool Button::isPressed() {
    return digitalRead(pin) == HIGH;
}

void Button::destroy() {
    delete this;
}
