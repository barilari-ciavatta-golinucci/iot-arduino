#include "VirtualProx.h"

VirtualProx::VirtualProx() {
    randomSeed(analogRead(A5));
    this->reset();
}

int VirtualProx::getDistance() {
    if (distance > MAX_DECREMENT_VALUE && random(DECREMENT_CASE) == 1) {
        distance -= random(MAX_DECREMENT_VALUE);
    }
    
    return distance;
}

void VirtualProx::reset() {
    distance = INITIAL_DISTANCE;    
}
