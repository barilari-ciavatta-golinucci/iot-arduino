#ifndef __PHYSICAL_PIR__
#define __PHYSICAL_PIR__

#include "Pir.h"

class PhysicalPir: public Pir {
    public:
        PhysicalPir(int pin);
        bool getPresence();
        void reset();
    private:
        unsigned int pin;
};

#endif
