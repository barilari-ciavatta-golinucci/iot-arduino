#ifndef __PHYSICAL_PROX__
#define __PHYSICAL_PROX__

#include "Prox.h"

class PhysicalProx: public Prox {
    public:
        PhysicalProx(int trig, int echo);        
        int getDistance();        
        void reset();

    private:
        byte trigPin;
        byte echoPin;
        int microsecondsToCentimeters(long microseconds);
};

#endif
