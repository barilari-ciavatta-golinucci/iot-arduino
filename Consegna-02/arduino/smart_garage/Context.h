#ifndef __CONTEXT__
#define __CONTEXT__

#include "Led.h"
#include "Button.h"
#include "Pir.h"
#include "Prox.h"
#include "VirtualPir.h"
#include "VirtualProx.h"
#include "PhysicalPir.h"
#include "PhysicalProx.h"

#define DIST_MAX 100
#define DIST_CLOSE 50
#define DIST_MIN 10

#define LDIST1_PIN 5
#define LDIST2_PIN 6
#define LR_PIN 11
#define TOUCH_PIN 8
#define CLOSE_PIN 9
#define PIR_PIN 2
#define PROX_TRIG_PIN 4
#define PROX_ECHO_PIN 3

class Context {
    public:
        static Context* getInstance();
        bool startProxMsg;
        bool closeDoorMsg;
        Led* ledLR;
        Led* ledLDIST1;
        Led* ledLDIST2;        
        Button* touchButton;
        Button* closeButton;
        Pir* pir;
        Prox* prox;        
    private:
        Context();
        static Context* instance;
};

#endif
