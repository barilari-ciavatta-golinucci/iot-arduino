#include "ParkAssistTask.h"

ParkAssistTask::ParkAssistTask() {
    context = Context::getInstance();
}

void ParkAssistTask::init(int period) {
    Task::init(period);
    state = WAITING_CAR;
    lastDistance = 0;
    waitingCarEntryFlag = false;
    parkingOkEntryFlag = true;
}

void ParkAssistTask::tick() {
    if (!context->startProxMsg) {
        return;
    }

    unsigned int distance = context->prox->getDistance();
    String serialCommand = "";

    this->computeLedPark(distance);

    switch (state) {
        case WAITING_CAR:
            if (!waitingCarEntryFlag) {
                this->computeLedPark(distance);
                waitingCarEntryFlag = true;
            }

            if (distance <= DIST_MAX) {
                waitingCarEntryFlag = false;
                state = PARK_ASSIST;
                Log::info("ParkAssistTask: WAITING_CAR -> PARK_ASSIST");
            }

            break;
        case PARK_ASSIST:            
            while (Serial.available()) {
                serialCommand += (char) Serial.read();
            }
            if (serialCommand == "STOP\n" && distance > DIST_CLOSE) {
                Serial.println("TOO_FAR");
            }

            if (distance > DIST_MAX) {
                state = WAITING_CAR;
                Log::info("ParkAssistTask: PARK_ASSIST -> WAITING_CAR");
                return;
            } else if (distance < DIST_MIN) {
                state = PARKING_OK;
                Log::info("ParkAssistTask: PARK_ASSIST -> PARKING_OK");
                return;
            }

            this->computeLedPark(distance);
            if ((distance < lastDistance - 2) || (distance > lastDistance + 2)) {
                Serial.println(distance);
                lastDistance = distance;
            }

            break;
        case PARKING_OK:
            if (!parkingOkEntryFlag) {
                this->computeLedPark(distance);
                Serial.println("OK_CAN_STOP");
                parkingOkEntryFlag = true;
            }

            while (Serial.available()) {
                serialCommand += (char) Serial.read();
            }
            if (serialCommand == "STOP\n" && distance < DIST_MIN) {
                Context::getInstance()->closeDoorMsg = true;
                Context::getInstance()->startProxMsg = false;
                state = WAITING_CAR;
                Log::info("ParkAssistTask: PARKING_OK -> WAITING_CAR");
                lastDistance = 0;
                parkingOkEntryFlag = false;                
                return;
            }

            if (distance >= DIST_MIN) {
                state = PARK_ASSIST;
                Log::info("ParkAssistTask: PARKING_OK -> PARK_ASSIST");
                parkingOkEntryFlag = false;
                return;
            }

            if (context->touchButton->isPressed()) {
                Serial.println("TOUCHING");
            }
            break;
    }
}

/*switch on led in function of the car distance*/
void ParkAssistTask::computeLedPark(unsigned int distance) {
    double value = distance / (double) DIST_MAX;    

    if (value > 0.5) {
        context->ledLDIST1->setIntensity((1.0 - value) * 2.0);
        context->ledLDIST2->setIntensity(0.0);
    } else {
        context->ledLDIST1->setIntensity(1.0);
        context->ledLDIST2->setIntensity(1.0 - value * 2.0);
    }
}
