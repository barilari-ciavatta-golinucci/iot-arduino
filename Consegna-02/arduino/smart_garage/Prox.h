/************************************************************************
 * ----------------------------Metodi Pubblici:------------------------ *
 *                                                                      *
 * Prox(int trig, int echo) -> Constructor, it wants trig pin value and *
 * echo pin value                                                       *
 *                                                                      *
 * int getDistance() -> return the distance in centimeters              *
 *                                                                      *
 * ----------------------------Metodi Privati:------------------------- *
 *                                                                      *
 * int microsecondsToCentimeters(long microseconds) -> Convert          *
 * microseconds in centimeters                                          *
 *                                                                      *
 ************************************************************************/

#ifndef __PROX__
#define __PROX__

#include "Arduino.h"

class Prox {
    public:
        virtual int getDistance() = 0;
        virtual void reset() = 0;
};

#endif
