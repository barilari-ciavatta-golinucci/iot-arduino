#ifndef __DOORGARAGETASK__
#define __DOORGARAGETASK__

#include "Task.h"
#include "Context.h"
#include "Log.h"
#include "Arduino.h"

#define LED_FADE_DURATION 2000
#define N 5000

class DoorGarageTask: public Task {
    enum State {CLOSED_DOOR, OPENING_DOOR, WAITING_CAR, CLOSING_DOOR, OPEN_DOOR};

    public:
        DoorGarageTask();
        void init(int period);
        void tick();

    private:     
        Context* context;
        State state;
        unsigned int elapsedTime;
        unsigned int periodTime;
        bool closingDoorEntryFlag;
        bool openDoorEntryFlag;
};

#endif
