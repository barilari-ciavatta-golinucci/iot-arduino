#include "Context.h"

Context* Context::instance = new Context();

Context* Context::getInstance() {
    return instance;
}

Context::Context() {
    closeDoorMsg = false;
    startProxMsg = false;
    ledLR = new Led(LR_PIN);
    ledLDIST1 = new Led(LDIST1_PIN);
    ledLDIST2 = new Led(LDIST2_PIN);    
    touchButton = new Button(TOUCH_PIN);
    closeButton = new Button(CLOSE_PIN);
    pir = new PhysicalPir(PIR_PIN); // new VirtualPir();
    prox = new PhysicalProx(PROX_TRIG_PIN, PROX_ECHO_PIN); // new VirtualProx();
}
