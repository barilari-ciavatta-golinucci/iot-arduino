/****************************************************************
 *                                                              *
 *   smart_garage.ino                                           *
 *                                                              *
 *   Written in 2017 by                                         *
 *   Nicolas Barilari <nicolas.barilari(at)studio.unibo.it>     *
 *   Emiliano Ciavatta <emiliano.ciavatta(at)studio.unibo.it>   *
 *   Simone Golinucci <simone.golinucci(at)studio.unibo.it>     *
 *                                                              *
 ****************************************************************/

#include "Scheduler.h"
#include "Task.h"
#include "DoorGarageTask.h"
#include "ParkAssistTask.h"

Scheduler scheduler;

void setup() {
    Serial.begin(9600);
    scheduler.init(50); //milliseconds
    Log::enableDebug(false);
    
    Task* task0 = new DoorGarageTask();
    task0->init(50);
    scheduler.addTask(task0);

    Task* task1 = new ParkAssistTask();
    task1->init(100);
    scheduler.addTask(task1);
}

void loop() {
    scheduler.schedule();
}
