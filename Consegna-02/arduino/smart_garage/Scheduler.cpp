#include "Scheduler.h"

void Scheduler::init(int basePeriod) {
    this->basePeriod = basePeriod;
    timer.setupPeriod(basePeriod);
    numTasks = 0;   
}

bool Scheduler::addTask(Task* task) {
    if (numTasks < NUM_TASKS) {
        taskList[numTasks] = task;
        numTasks++;
        return true;
    }
    return false;
}

void Scheduler::schedule() {
    timer.waitForNextTick();
    for (int i = 0; i < numTasks; i++) {
        if (taskList[i]->updateAndCheckTime(basePeriod)) {
            taskList[i]->tick();            
        }
    }
}
