/************************************************************************
 * ----------------------------Metodi Pubblici:------------------------ *
 *                                                                      *
 * Pir(int pin) -> Constructor, this initialize the pin where Pir write *
 * the HIGT signal                                                      *
 *                                                                      *
 * bool getPresence() -> return true if Pir found a nPresence otherwise *
 * return false                                                         *
 *                                                                      *
 *************************************************************************/

#ifndef __PIR__
#define __PIR__

#include "Arduino.h"

class Pir {
    public:
        virtual bool getPresence() = 0;
        virtual void reset() = 0;
};

#endif
