package smart_garage;

/**
 * 
 * All available states.
 */
public enum State {

	DOOR_CLOSED, WAITING_GREETING, CAN_STOP

}
