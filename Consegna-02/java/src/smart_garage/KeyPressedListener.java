package smart_garage;

/**
 * 
 * 
 */
public interface KeyPressedListener {

	/**
	 * Invoked when a key is pressed.
	 */
	void onKeyPressed(char key);

}
