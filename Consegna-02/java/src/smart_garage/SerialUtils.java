package smart_garage;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import gnu.io.CommPortIdentifier;

/**
 * 
 * Static class with some serial utility functions.
 */
public final class SerialUtils {

	/**
	 * 
	 * @return a list with all the serial ports available on the PC
	 */
	public static List<String> getAvailableSerialPorts() {
		final List<String> ports = new ArrayList<>();

		Enumeration<?> portEnum = CommPortIdentifier.getPortIdentifiers();
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier portId = (CommPortIdentifier) portEnum.nextElement();
			ports.add(portId.getName());
		}

		return ports;
	}

}
