package smart_garage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 
 * It represents a console terminal.
 */
public final class Terminal implements KeyPressedListener {

	private final static int DELAY = 500;
	
	private final List<KeyPressedListener> listeners = new ArrayList<>();
	private final BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(10);
	
	public Terminal() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					
					while (true) {
						String line = br.readLine();
						if (line.length() == 1) {
							listeners.forEach(l -> l.onKeyPressed(line.charAt(0)));
						}
					}					
				} catch (IOException e) {
				} // Not expected
			}
		}).start();
		
		this.registerKeyPressedHandler(this);
	}

	/**
	 * 
	 * @param str the message
	 */
	public void output(final String str) {
		System.out.print(str);
		System.out.flush();
	}

	/**
	 * 
	 * @param str the message
	 */
	public void error(final String str) {
		System.err.print(str);
		System.err.flush();
	}

	/**
	 * 
	 * @param str the message
	 */
	public void outputln(final String str) {
		System.out.println(str);
		System.out.flush();
	}

	/**
	 * 
	 * @param str the message
	 */
	public void errorln(final String str) {
		System.err.println(str);
		System.err.flush();
	}

	/**
	 * 
	 * @param milliseconds the number of milliseconds to sleep.
	 */
	public void sleepMilliseconds(final int milliseconds) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				int time = 0;
				while (time + DELAY < milliseconds) {
					Terminal.this.output(".");
					time += DELAY;
					try {
						Thread.sleep(DELAY);
					} catch (InterruptedException e) {
					} // Not expected
				}
			}
		}).start();

		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
		} // Not expected
		this.outputln("");
	}

	/**
	 * 
	 * @param question the question
	 * @param answers a list with all possible answers
	 * @return an answers
	 */
	public String makeQuestionAndGetAnswer(final String question, final List<String> answers) {
		this.outputln(question);
		for (int i = 1; i <= answers.size(); i++) {
			this.outputln("\t[" + i + "] " + answers.get(i - 1));
		}

		int answer = -1;
		while (answer == -1) {			
			try {
				this.output("Select a number: ");
				answer = queue.take();
				if (answer <= 0 || answer > answers.size()) {
					answer = -1;
					this.errorln("Invalid Range. Please enter a valid number!");
				}
			} catch (InterruptedException e) {
			} // Not expected
		}

		return answers.get(answer - 1);
	}

	/**
	 * 
	 * @param listener the handler
	 */
	public void registerKeyPressedHandler(final KeyPressedListener listener) {
		listeners.add(listener);
	}

	@Override
	public void onKeyPressed(char key) {
		try {
			if (key != 's') {
				queue.put(key - '0');
			}			
		} catch (InterruptedException e) {			
		} // Not expected
	}

}
