package smart_garage;

import java.util.Arrays;
import java.util.List;

/**
 * The main class which manages the logic of the console.
 */
public final class CarConsole implements KeyPressedListener {

	private SerialCommChannel channel;
	private Terminal terminal = new Terminal();
	private State state = State.DOOR_CLOSED;
	private boolean stopEnabled = false;

	private CarConsole() {
	}

	/**
	 * Entry point of Auto Console.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		final CarConsole console = new CarConsole();
		final String port = console.choosePortAndGet();
		if (!console.initConnection(port)) {
			return;
		}
		
		while (console.doAction());
		console.closeConnection();
		System.exit(0);
	}

	/**
	 * 
	 * @return the serial port of Arduino
	 */
	private String choosePortAndGet() {
		final String question = "Choose the serial port of the car";
		final List<String> answers = SerialUtils.getAvailableSerialPorts();

		return terminal.makeQuestionAndGetAnswer(question, answers);
	}

	/**
	 * 
	 * @param serialPort
	 *            the serial port of Arduino
	 * @return true if a connection has been established
	 */
	private boolean initConnection(final String serialPort) {
		try {
			channel = new SerialCommChannel(serialPort, 9600);
			terminal.output("Waiting for setting up the connection");
			terminal.sleepMilliseconds(4000);
			terminal.registerKeyPressedHandler(this);
			return true;
		} catch (Exception e) {
			terminal.errorln("An error occurred: " + e.getMessage());
			return false;
		}
	}

	/**
	 * 
	 * @return true if the loop must continue
	 */
	private boolean doAction() {
		switch (state) {
		case DOOR_CLOSED:
			final String question = "What do you want to do?";
			final List<String> answers = Arrays.asList("Open door", "Exit");
			final String answer = terminal.makeQuestionAndGetAnswer(question, answers);

			if (answer.equals("Open door")) {
				channel.sendMsg("OPEN");
				terminal.output("The door is opening");
				terminal.sleepMilliseconds(2000);
				terminal.outputln("You can come in, the door is open");
				state = State.WAITING_GREETING;
				return true;
			}
			return false;
		case WAITING_GREETING:
			try {
				final String msg = channel.receiveMsg();
				if (msg.equals("WELCOME_HOME")) {
					terminal.outputln("Welcome Home.");
					terminal.outputln("When the car is off, type [s] followed by new line to close the door");
					stopEnabled = true;
					state = State.CAN_STOP;
					return true;
				} else if (msg.equals("NOBODY")) {
					terminal.outputln("Nobody detected. Closing door");
					terminal.sleepMilliseconds(2000);
					terminal.outputln("The door is now closed!");
					state = State.DOOR_CLOSED;
					return true;
				}

				terminal.errorln("Unexpected message: " + msg);
				return false;
			} catch (InterruptedException e) {
				terminal.errorln("Unexpected error: " + e.getMessage());
				return false;
			}
		case CAN_STOP:
			String msg = "";
			try {
				msg = channel.receiveMsg();
				if (msg.equals("OK_CAN_STOP")) {
					terminal.outputln("You can turn off the car.");
				} else if (msg.equals("TOO_FAR")) {
					terminal.outputln("You can not turn off the car, it's too far.");
				} else if (msg.equals("TOUCHING")) {
					terminal.outputln("Ouch! You touched the wall.");
				} else if (msg.equals("OK")) {
					terminal.output("The door is closing");
					terminal.sleepMilliseconds(2000);
					terminal.outputln("The door is now closed!");
					stopEnabled = false;
					state = State.DOOR_CLOSED;					
				} else {

					int distance = Integer.parseInt(msg);
					terminal.outputln("Distance from wall (in centimeters): " + distance);
				}
				return true;
			} catch (NumberFormatException e) {
				terminal.errorln("Unexpected message: " + msg);
				return false;
			} catch (InterruptedException e) {
				terminal.errorln("Unexpected error: " + e.getMessage());
				return false;
			}
		default:
			return false;
		}
	}

	/**
	 * Close the serial connection.
	 */
	private void closeConnection() {
		channel.close();
		terminal.outputln("Connection closed. Bye");
	}

	@Override
	public void onKeyPressed(final char key) {
		if (stopEnabled && key == 's') {
			channel.sendMsg("STOP");
			terminal.outputln("Stop signal sent");
		}		
	}

}
