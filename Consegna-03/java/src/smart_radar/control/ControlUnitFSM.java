package smart_radar.control;

import smart_radar.control.state.StateController;
import smart_radar.events.Event;

/**
 * Manages the Finite State Machine by checking states and deciding what to do.
 */
public class ControlUnitFSM extends EventLoopController {	
	private StateController context;
	
	public ControlUnitFSM(StateController context) {
		super();
		this.context = context;
	}

	@Override
	protected void processEvent(Event event) {
		this.context.toDo(event);
	}
}
