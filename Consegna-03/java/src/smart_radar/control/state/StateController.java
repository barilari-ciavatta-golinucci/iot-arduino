package smart_radar.control.state;

import smart_radar.common.Context;
import smart_radar.events.Event;

public class StateController {
	private State state;
	
	public StateController(Context config){
		this.setState(new IdleState(this, config));
	}
	
	public void setState(final State nextState) {
		this.state = nextState;
	}
	
	public void toDo(Event event) {
		this.state.toDo(event);
	}
}
