package smart_radar.control.state;

import smart_radar.common.Context;
import smart_radar.events.ButtonPressedEvent;
import smart_radar.events.Event;

public class IdleState implements State {
	private final StateController controller;
	private final Context context;
	
	public IdleState(StateController controller, Context context) {
		this.controller = controller;
		this.context = context;
		context.getLed(Context.LED_ON).switchOff();
		context.getLed(Context.LED_DETECTED).switchOff();
		context.getLed(Context.LED_TRACKING).switchOff();
	}

	@Override
	public void toDo(Event event) {
		if (event instanceof ButtonPressedEvent) {
			if (((ButtonPressedEvent) event).getName().equals(Context.BTN_ON)) {
				this.context.getSerial().sendMessage(Context.MSG_ON + Context.SEPARATOR + Context.ANGULAR_VELOCITY);
				this.context.getLed(Context.LED_ON).switchOn();
				this.context.setObj(0);
				this.context.setObjDetected(false);
				
				this.controller.setState(new WorkingState(controller, context));
			}				
		}
	}
}
