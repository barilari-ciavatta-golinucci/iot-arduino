package smart_radar.control.state;

import smart_radar.events.Event;

public interface State {
	/**
	 * Run the state task
	 */
	void toDo(Event event);
}
