package smart_radar.control.state;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import smart_radar.common.Context;
import smart_radar.events.ButtonPressedEvent;
import smart_radar.events.Event;
import smart_radar.events.MessageReceivedEvent;

public class TrackingState implements State {
	private static final Pattern DISTANCE_ANGLE_PATTERN = Pattern.compile("^(\\d+);(\\d+)$");
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");
	private final StateController controller;
	private final Context context;
	private boolean entryFlag = false;
	
	public TrackingState(StateController controller, Context context) {
		this.controller = controller;
		this.context = context;
	}

	@Override
	public void toDo(Event event) {
		if (event instanceof ButtonPressedEvent) {
			if (((ButtonPressedEvent) event).getName().equals(Context.BTN_OFF)) {
				this.context.getSerial().sendMessage(Context.MSG_OFF);

				controller.setState(new IdleState(controller, context));
			}				
		} else if (event instanceof MessageReceivedEvent) {
			String message = ((MessageReceivedEvent) event).getMessage();
			Matcher matcher = DISTANCE_ANGLE_PATTERN.matcher(message);
			if (matcher.find()) {
				int distance = Integer.parseInt(matcher.group(1));
				int angle = Integer.parseInt(matcher.group(2));
				if (distance < Context.MIN_DIST && !entryFlag) {
					this.context.getLed(Context.LED_TRACKING).switchOn();
					this.context.getSerial().sendMessage(Context.MSG_STOP);
					this.context.getTerminal().outputln("Time " + DATE_FORMAT.format(new Date()) +
							" - Object tracked at angle " + angle + " distance " + distance);
					this.context.setObj(this.context.getObj() + 1);
					entryFlag = true;
				} else if (distance >= Context.MIN_DIST) {
					this.context.getLed(Context.LED_TRACKING).switchOff();
					this.context.getSerial().sendMessage(Context.MSG_START);
					
					controller.setState(new WorkingState(controller, context));
				}
			}
		}
	}

}
