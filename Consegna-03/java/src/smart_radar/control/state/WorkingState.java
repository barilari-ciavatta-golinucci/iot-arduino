package smart_radar.control.state;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import smart_radar.common.Context;
import smart_radar.events.ButtonPressedEvent;
import smart_radar.events.Event;
import smart_radar.events.MessageReceivedEvent;
import smart_radar.utils.Log;

public class WorkingState implements State {
	private static final Pattern DISTANCE_ANGLE_PATTERN = Pattern.compile("^(\\d+);(\\d+)$");
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");
	private final StateController controller;
	private final Context context;
	
	public WorkingState(StateController controller, Context context) {
		this.controller = controller;
		this.context = context;
	}

	@Override
	public void toDo(Event event) {
		if (event instanceof ButtonPressedEvent) {
			if (((ButtonPressedEvent) event).getName().equals(Context.BTN_OFF)) {
				this.context.getSerial().sendMessage(Context.MSG_OFF);

				this.controller.setState(new IdleState(controller, context));
			}				
		} else if (event instanceof MessageReceivedEvent) {
			String message = ((MessageReceivedEvent) event).getMessage();
			if (message.equals(Context.MSG_ROUND)) {
				this.context.getTerminal().outputln("Objects detected on this round: " +
						(this.context.getObj() + (this.context.isObjDetected() ? 1 : 0)));
				this.context.setObj(0);
				this.context.setObjDetected(false);
				return;
			}
			
			Matcher matcher = DISTANCE_ANGLE_PATTERN.matcher(message);
			if (!matcher.find()) {
				return;
			}
			int distance = Integer.parseInt(matcher.group(1));
			int angle = Integer.parseInt(matcher.group(2));
			if (distance < Context.MIN_DIST) {
				this.controller.setState(new TrackingState(controller, context));
			} else if (Context.MIN_DIST <= distance && distance <= Context.MAX_DIST && !this.context.isObjDetected()) {
				this.context.getLed(Context.LED_DETECTED).blink(Context.BLINK_DELAY, Context.BLINK_DURATION);
				String output = "Time " + DATE_FORMAT.format(new Date()) + " - Object detected at angle " + angle;
				this.context.getTerminal().outputln(output);
				Log.getLogger().info(output);
				this.context.setObjDetected(true);
			} else if (distance > Context.MAX_DIST && this.context.isObjDetected()) {
				this.context.setObj(this.context.getObj() + 1);
				this.context.setObjDetected(false);
			}			
		}
	}
}
