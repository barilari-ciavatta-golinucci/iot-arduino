package smart_radar.utils;

import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * The logger utility of the application.
 */
public final class Log {
    private static final Logger LOGGER = Logger.getLogger("SMART_RADAR");
    private static boolean isInitialized;

    private Log() {
    }

    /**
     * @return the only instance of the logger
     */
    public static Logger getLogger() {
        if (!isInitialized) {
            setupLogger();
        }

        return LOGGER;
    }

    private static void setupLogger() {
        LogManager.getLogManager().reset();
        FileHandler handler = null;
        try {
			handler = new FileHandler("logs.log");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

        handler.setFormatter(new Formatter() {
            @Override
            public String format(final LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        handler.setLevel(Level.ALL);

        LOGGER.addHandler(handler);
        LOGGER.setLevel(Level.ALL);

        isInitialized = true;
    }
}
