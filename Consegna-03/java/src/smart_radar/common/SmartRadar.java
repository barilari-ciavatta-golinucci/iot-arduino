package smart_radar.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import smart_radar.control.ControlUnitFSM;
import smart_radar.control.EventLoopController;
import smart_radar.control.state.StateController;
import smart_radar.devices.Serial;
import smart_radar.devices.Terminal;
import smart_radar.devices.Button;
import smart_radar.devices.Led;
import smart_radar.events.ButtonPressedEvent;
import smart_radar.events.ButtonPressedListener;
import smart_radar.events.MessageReceivedEvent;
import smart_radar.events.MessageReceivedListener;
import smart_radar.utils.SerialUtils;

/**
 * Class which starts the program.
 */
public class SmartRadar {	
	private final Terminal terminal = new Terminal();
	private EventLoopController eventLoop;
	private Serial serial;	
			
	public static void main(String[] args) {
		SmartRadar smartRadar = new SmartRadar();
		smartRadar.welcome();
		String port = smartRadar.choosePortAndGet();
		if (smartRadar.initConnection(port)) {
			smartRadar.terminal.outputln("Connected to the radar. Press CTRL + C to exit");
			smartRadar.start();
		} else {
			smartRadar.terminal.errorln("Error: can't connect to the radar.");
		}
	}
	
	private void start() {
		Map<String, Button> buttons = new HashMap<>();
		Map<String, Led> leds = new HashMap<>();
		
		/*creazione pulsanti*/
		Button buttonOn = new Button(4, Context.BTN_ON);
		buttons.put(Context.BTN_ON, buttonOn);
		Button buttonOff = new Button(5, Context.BTN_OFF);
		buttons.put(Context.BTN_OFF, buttonOff);

		/*creazione led*/
		leds.put(Context.LED_ON, new Led(0, Context.LED_ON));
		leds.put(Context.LED_DETECTED, new Led(2, Context.LED_DETECTED));
		leds.put(Context.LED_TRACKING, new Led(3, Context.LED_TRACKING));
		
		/*creazione eventLoop*/
		Context context = new Context(buttons, leds, serial, this.terminal);
		this.eventLoop = new ControlUnitFSM(new StateController(context));
		this.eventLoop.start();
		
		/*creazione evento bottoni*/
		ButtonPressedListener bpl = new ButtonPressedListener() {			
			@Override
			public void onButtonPressed(String buttonName) {
				eventLoop.notifyEvent(new ButtonPressedEvent(buttonName));
			}
		};
		buttonOn.addButtonPressedListener(bpl);
		buttonOff.addButtonPressedListener(bpl);

		/*creazione evento serial*/
		this.serial.addMessageReceivedListener(new MessageReceivedListener() {			
			@Override
			public void onMessageReceived(String message) {
				eventLoop.notifyEvent(new MessageReceivedEvent(message));
			}
		});
		
		this.clearResourcesOnClose();
	}
	
	private void welcome() {
		this.terminal.outputln("<< Welcome to the Smart Radar >>");
	}
	
	/**
	 * 
	 * @return the serial port of Arduino
	 */
	private String choosePortAndGet() {
		String question = "Choose the serial port of the radar";
		List<String> answers = SerialUtils.getAvailableSerialPorts();

		return this.terminal.makeQuestionAndGetAnswer(question, answers);
	}

	/**
	 * 
	 * @param serialPort
	 *            the serial port of Arduino
	 * @return true if a connection has been established
	 */
	private boolean initConnection(final String serialPort) {
		try {
			this.serial = new Serial(serialPort, 9600);			
		} catch (Exception e) {
			terminal.errorln("An error occurred: " + e.getMessage());
			return false;
		}
		
		this.terminal.output("Waiting for setting up the connection with the radar");
		this.terminal.sleepMilliseconds(4000);
		return true;
	}
	
	private void clearResourcesOnClose() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
		    public void run() {
		    	eventLoop.notifyEvent(new ButtonPressedEvent(Context.BTN_OFF));
		    	terminal.outputln("Connection to the radar closed. Bye");
	    	}
		});
	}
}
