package smart_radar.common;

import java.util.Map;

import smart_radar.devices.Button;
import smart_radar.devices.Led;
import smart_radar.devices.Serial;
import smart_radar.devices.Terminal;

public class Context {
	public static final String BTN_ON = "BTN_ON";
	public static final String BTN_OFF = "BTN_OFF";
	public static final String LED_ON = "LED_ON";
	public static final String LED_DETECTED = "LED_DETECTED";
	public static final String LED_TRACKING = "LED_TRACKING";
	public static final int ANGULAR_VELOCITY = 10;
	public static final int MIN_DIST = 30;
	public static final int MAX_DIST = 100;
	public static final int BLINK_DELAY = 100;
	public static final int BLINK_DURATION = 100;
	public static final String SEPARATOR = ";";
	public static final String MSG_ON = "ON";
	public static final String MSG_OFF = "OFF";
	public static final String MSG_START = "START";
	public static final String MSG_STOP = "STOP";
	public static final String MSG_ROUND = "ROUND";
	
	private final Map<String, Button> buttons;
	private final Map<String, Led> leds;
	private final Serial serial;
	private final Terminal terminal;
	private int obj = 0;
	private boolean objDetected = false;
	
	public Context(Map<String, Button> buttons, Map<String, Led> leds, Serial serial, Terminal terminal) {
		this.buttons = buttons;
		this.leds = leds;
		this.serial = serial;
		this.terminal = terminal;
	}
	
	public Button getButton(String buttonName) {
		return this.buttons.get(buttonName);
	}
	
	public Led getLed(String ledName) {
		return this.leds.get(ledName);
	}
	
	public Serial getSerial() {
		return this.serial;		
	}
	
	public Terminal getTerminal() {
		return this.terminal;		
	}
	
	public int getObj() {
		return this.obj;
	}
	
	public void setObj(int value) {
		this.obj = value;
	}
	
	public boolean isObjDetected() {
		return this.objDetected;
	}
	
	public void setObjDetected(boolean objDetected) {
		this.objDetected = objDetected;
	}
}
