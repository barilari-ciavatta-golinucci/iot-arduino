package smart_radar.events;

public interface ButtonPressedListener {
	
	void onButtonPressed(String buttonName);	
}
