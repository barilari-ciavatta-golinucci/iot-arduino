package smart_radar.events;

public interface MessageReceivedListener {
	
	void onMessageReceived(String message);
}
