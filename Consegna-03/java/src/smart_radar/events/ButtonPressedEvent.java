package smart_radar.events;

public class ButtonPressedEvent extends Event {
	private String name;	

	public ButtonPressedEvent(String name) {
		super();
		this.name = name;		
	}
	
	public String getName() {
		return this.name;
	}
}
