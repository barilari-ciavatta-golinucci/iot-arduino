package smart_radar.devices;

import smart_radar.events.ButtonPressedListener;

import java.util.ArrayList;
import java.util.List;

import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

public class Button implements Clickable {
	private final GpioPinDigitalInput button;
	private final String name;
	private final List<ButtonPressedListener> listeners = new ArrayList<>();
	
	public Button(int pinNumber, String name) {
		this.button = GpioFactory.getInstance().provisionDigitalInputPin(RaspiPin.getPinByAddress(pinNumber), name, PinPullResistance.PULL_DOWN);
		this.button.addListener(new GpioPinListenerDigital() {
	        @Override
	        public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {	        	
	        	if (event.getState() == PinState.HIGH) { //only for HIGH pin state
	        		listeners.forEach(l -> l.onButtonPressed(name));
	        	}
	        }
	    });
		this.name = name;
	}

	@Override
	public boolean isPressed() {
		return this.button.isHigh();
	}
	
	public void addButtonPressedListener(ButtonPressedListener listener) {
		listeners.add(listener);
	}
	
	public String getName() {
		return name;
	}
}
