package smart_radar.devices;

import java.util.List;
import java.util.Scanner;

/**
 * 
 * It represents a console terminal.
 */
public class Terminal {
	private static final int DELAY = 500;

	/**
	 * 
	 * @param str the message
	 */
	public void output(String str) {
		System.out.print(str);
		System.out.flush();
	}

	/**
	 * 
	 * @param str the message
	 */
	public void error(String str) {
		System.err.print(str);
		System.err.flush();
	}

	/**
	 * 
	 * @param str the message
	 */
	public void outputln(String str) {
		System.out.println(str);
		System.out.flush();
	}

	/**
	 * 
	 * @param str the message
	 */
	public void errorln(String str) {
		System.err.println(str);
		System.err.flush();
	}

	/**
	 * 
	 * @param milliseconds the number of milliseconds to sleep.
	 */
	public void sleepMilliseconds( int milliseconds) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				int time = 0;
				while (time + DELAY < milliseconds) {
					Terminal.this.output(".");
					time += DELAY;
					try {
						Thread.sleep(DELAY);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.outputln("");
	}

	/**
	 * 
	 * @param question the question
	 * @param answers a list with all possible answers
	 * @return an answers
	 */
	public String makeQuestionAndGetAnswer(String question, List<String> answers) {
		this.outputln(question);
		for (int i = 1; i <= answers.size(); i++) {
			this.outputln("\t[" + i + "] " + answers.get(i - 1));
		}
		
		Scanner s = new Scanner(System.in);
		int answer = -1;
		
		while (answer == -1) {			
			this.output("Select a number: ");
			if (s.hasNextInt()) {
				answer = s.nextInt();
			} else {
				s.next();
				answer = -1;
			}
			
			if (answer <= 0 || answer > answers.size()) {
				answer = -1;
				this.errorln("Invalid Range. Please enter a valid number!");
			}
		}		
		s.close();

		return answers.get(answer - 1);
	}

}
