package smart_radar.devices;

/**
 * Represents a light which can be switched on or switched off.
 */
public interface Light {
	
	/**
	 * Switch on the light.
	 */
	void switchOn();
	
	/**
	 * Switch off the light.
	 */
	void switchOff();
	
	/**
	 * Toggle the light.
	 */
	void toggle();
}
