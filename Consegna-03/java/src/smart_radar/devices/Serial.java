package smart_radar.devices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import smart_radar.events.MessageReceivedListener;

public class Serial {	
	private SerialPort serialPort;
	private BufferedReader input;
	private OutputStream output;
	private final List<MessageReceivedListener> listeners = new ArrayList<>();

	public Serial(String port, int rate) {
		try {
			CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(port);
			// open serial port, and use class name for the appName.
			this.serialPort = (SerialPort) portId.open(this.getClass().getName(), 2000);
			// set port parameters
			this.serialPort.setSerialPortParams(rate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			// open the streams
			this.input = new BufferedReader(new InputStreamReader(this.serialPort.getInputStream()));
			this.output = this.serialPort.getOutputStream();
			// add event listeners
			this.serialPort.addEventListener(new SerialPortEventListener() {			
				@Override
				public void serialEvent(SerialPortEvent event) {
					if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {						
						try {
							String message = input.readLine();
							listeners.forEach(l -> l.onMessageReceived(message));							
						} catch (IOException e) {
							e.printStackTrace();
						}
					}					
				}
			});
			this.serialPort.notifyOnDataAvailable(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(String message) {
		char[] array = (message + "\n").toCharArray();
		byte[] bytes = new byte[array.length];
		for (int i = 0; i < array.length; i++) {
			bytes[i] = (byte) array[i];
		}
		try {
			this.output.write(bytes);
			this.output.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This should be called when you stop using the port.
	 * This will prevent port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (this.serialPort != null) {
			this.serialPort.removeEventListener();
			this.serialPort.close();
		}
	}
	
	public void addMessageReceivedListener(MessageReceivedListener listener) {
		listeners.add(listener);
	}
}
