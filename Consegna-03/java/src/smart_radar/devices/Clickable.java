package smart_radar.devices;

/**
 *	Represents an object which can be clicked or pressed.
 */
public interface Clickable {

	/**
	 * Specifies if the object is pressed.
	 * @return true if the object is pressed, false otherwise.
	 */
	public boolean isPressed();
}
