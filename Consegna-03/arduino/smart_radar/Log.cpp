#include "Log.h"

bool Log::isDebugEnabled = false;

bool Log::error(String message) {
    if (Log::isDebugEnabled) {
        Serial.print("DEBUG [ERROR]: ");
        Serial.println(message);
        return true;
    } else {
        return false;
    }
}

bool Log::warning(String message) {
    if (Log::isDebugEnabled) {
        Serial.print("DEBUG [WARNING]: ");
        Serial.println(message);
        return true;
    } else {
        return false;
    }
}

bool Log::info(String message) {
    if (Log::isDebugEnabled) {
        Serial.print("DEBUG [INFO]: ");
        Serial.println(message);
        return true;
    } else {
        return false;
    }
}

void Log::enableDebug(boolean flag) {
    isDebugEnabled = flag;
}
