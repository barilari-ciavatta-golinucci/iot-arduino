#ifndef __SCHEDULER__
#define __SCHEDULER__

#include "Timer.h"
#include "Task.h"

#define NUM_TASKS 10

class Scheduler {
    public:
        void init(int basePeriod);
        bool addTask(Task* task);
        void schedule();
    private:
        int basePeriod;
        int numTasks;
        Task* taskList[NUM_TASKS];
        Timer timer;
};

#endif
