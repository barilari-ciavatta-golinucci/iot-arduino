#ifndef __LOG__
#define __LOG__

#include "Arduino.h"

class Log {
    public:
        static bool error(String message);
        static bool warning(String message);
        static bool info(String message);
        static void enableDebug(boolean flag);

    private:
        static bool isDebugEnabled;
};

#endif
