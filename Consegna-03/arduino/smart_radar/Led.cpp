#include "Led.h"

#define MAX_INTENSITY 255

Led::Led(unsigned int pin) {
    this->pin = pin;
    this->isSwitchedOn = false;
    this->currIntensity = 0;
    pinMode(pin, OUTPUT);
}

void Led::switchOn() {
    digitalWrite(this->pin, HIGH);
    this->isSwitchedOn = true;
    this->currIntensity = 0;
}

void Led::switchOff() {
    digitalWrite(this->pin, LOW);
    this->isSwitchedOn = false;
    this->currIntensity = 0;
};

void Led::toggle() {
    if (this->isSwitchedOn) {
        this->switchOff();
    } else {
        this->switchOn();
    }
};

void Led::incrementFade(unsigned int fadeAmount) {
    if (this->currIntensity + fadeAmount <= MAX_INTENSITY) {
        this->currIntensity += fadeAmount;
    } else {
        this->currIntensity = MAX_INTENSITY;
    }
    this->updateIntensity();
}

void Led::decrementFade(unsigned int fadeAmount) {
    if (this->currIntensity - fadeAmount >= 0) {
        this->currIntensity -= fadeAmount;
    } else {
        this->currIntensity = 0;
    }
    this->updateIntensity();
}

void Led::setIntensity(double intensity) {
    if (intensity < 0) {
        intensity = 0;
    } else if (intensity > 1) {
        intensity = 1;
    }
    this->currIntensity = (unsigned int) (MAX_INTENSITY * intensity);
    this->updateIntensity();
}

bool Led::isMinIntensityReached() {
    return this->currIntensity == 0;
}

bool Led::isMaxIntensityReached() {
    return this->currIntensity == MAX_INTENSITY;
}

void Led::destroy() {
    delete this;
}

void Led::updateIntensity() {
    analogWrite(this->pin, this->currIntensity);
    this->isSwitchedOn = false;
}
