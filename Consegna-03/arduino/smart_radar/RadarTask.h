#ifndef __PARKASSISTTASK__
#define __PARKASSISTTASK__

#include "Arduino.h"
#include <Servo.h>
#include "Task.h"
#include "Led.h"
#include "Prox.h"
#include "Log.h"

class RadarTask: public Task {
    enum State {IDLE, SCANNING, TRACKING};

    public:
        RadarTask();
        void init(int period);
        void tick();

    private:
        State state;
        Servo servoMotor;
        Led *ledConnected;
        Prox *prox;
        unsigned int period;
        float alpha;
        float increment;
        bool idleEntryFlag;
};

#endif
