#ifndef __PROX__
#define __PROX__

#include "Arduino.h"

class Prox {
    public:
        Prox(int trig, int echo);
        int getDistance();

    private:
        byte trigPin;
        byte echoPin;
        int microsecondsToCentimeters(long microseconds);
};

#endif
