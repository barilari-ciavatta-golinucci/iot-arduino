#include "RadarTask.h"

#define SERVO_PIN 9
#define LED_CONNECTED_PIN 6
#define ECHO_PIN 3
#define TRIG_PIN 4

RadarTask::RadarTask() {
    servoMotor.attach(SERVO_PIN);
    ledConnected = new Led(LED_CONNECTED_PIN);
    prox = new Prox(TRIG_PIN, ECHO_PIN);
}

void RadarTask::init(int period) {
    Task::init(period);
    this->period = period;
    state = IDLE;
    idleEntryFlag = false;
}

void RadarTask::tick() {
    String serialCommand = "";

    switch (state) {
        case IDLE:
            if (!idleEntryFlag) {
                alpha = 90.0;
                servoMotor.write(round(alpha));
                ledConnected->switchOff();
                idleEntryFlag = true;
            }

            while (Serial.available()) {
                serialCommand += (char) Serial.read();
            }

            if (serialCommand.startsWith("ON")) {
                float omega = serialCommand.substring(3, serialCommand.length() - 1).toFloat();
                increment = omega / 1000 * (float) period;
                ledConnected->switchOn();
                idleEntryFlag = false;
                state = SCANNING;
                Log::info("RadarTask: IDLE -> SCANNING");
            }

            break;
        case SCANNING:
            while (Serial.available()) {
                serialCommand += (char) Serial.read();
            }

            if (serialCommand == "OFF\n") {
                state = IDLE;
                Log::info("RadarTask: SCANNING -> IDLE");
                return;
            } else if (serialCommand == "STOP\n") {
                state = TRACKING;
                Log::info("RadarTask: SCANNING -> TRACKING");
                return;
            }

            if (alpha + increment <= 0.0 || alpha + increment >= 180.0) {
                increment *= -1.0;
                Serial.println("ROUND");
            }

            alpha += increment;
            servoMotor.write(round(alpha));
            Serial.println(String(prox->getDistance()) + ";" + String(round(alpha)));

            break;
        case TRACKING:
            while (Serial.available()) {
                serialCommand += (char) Serial.read();
            }

            if (serialCommand == "OFF\n") {
                state = IDLE;
                Log::info("RadarTask: TRACKING -> IDLE");
                return;
            } else if (serialCommand == "START\n") {
                state = SCANNING;
                Log::info("RadarTask: TRACKING -> SCANNING");
                return;
            }

            Serial.println(String(prox->getDistance()) + ";" + String(round(alpha)));

            break;
    }
}
