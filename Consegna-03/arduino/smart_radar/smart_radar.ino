/****************************************************************
 *                                                              *
 *   smart_radar.ino                                           *
 *                                                              *
 *   Written in 2017 by                                         *
 *   Nicolas Barilari <nicolas.barilari(at)studio.unibo.it>     *
 *   Emiliano Ciavatta <emiliano.ciavatta(at)studio.unibo.it>   *
 *   Simone Golinucci <simone.golinucci(at)studio.unibo.it>     *
 *                                                              *
 ****************************************************************/

#include "Scheduler.h"
#include "Task.h"
#include "RadarTask.h"
#include "Log.h"

Scheduler scheduler;

void setup() {
    Serial.begin(9600);
    scheduler.init(100); //milliseconds
    Log::enableDebug(false);

    Task* radarTask = new RadarTask();
    radarTask->init(100);
    scheduler.addTask(radarTask);
}

void loop() {
    scheduler.schedule();
}
