#include "Timer.h"

volatile bool timerFlag;

void interrupt(void) {
    timerFlag = true;
}

Timer::Timer() {
    timerFlag = false;
}

void Timer::setupPeriod(int period) { // period in ms
    FlexiTimer2::set(period, 1.0/1000, interrupt);
    FlexiTimer2::start();
}

void Timer::waitForNextTick() {
    while (!timerFlag) {} // wait for timer signal
    timerFlag = false;
}
