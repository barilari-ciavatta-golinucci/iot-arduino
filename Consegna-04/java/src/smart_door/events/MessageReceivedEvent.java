package smart_door.events;

public class MessageReceivedEvent extends Event {
    private final String message;

    public MessageReceivedEvent(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
