package smart_door.events;

public interface MessageReceivedListener {

    void onMessageReceived(String message);
}
