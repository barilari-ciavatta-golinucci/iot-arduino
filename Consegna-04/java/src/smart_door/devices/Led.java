package smart_door.devices;

import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

public class Led implements Light {
    private final GpioPinDigitalOutput led;
    private final String name;

    public Led(int pinNumber, String name) {
        this.led = GpioFactory.getInstance().provisionDigitalOutputPin(RaspiPin.getPinByAddress(pinNumber), name,
                PinState.LOW);
        this.led.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
        this.name = name;
    }

    @Override
    public void switchOn() {
        this.led.high();
    }

    @Override
    public void switchOff() {
        this.led.low();
    }

    @Override
    public void toggle() {
        if (this.led.getState() == PinState.HIGH) {
            this.led.low();
        } else {
            this.led.high();
        }
    }

    public void blink(int delay, int duration) {
        this.led.blink(delay, duration);
    }

    public String getName() {
        return name;
    }
}
