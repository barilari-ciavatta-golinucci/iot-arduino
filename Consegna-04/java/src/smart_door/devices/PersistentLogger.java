package smart_door.devices;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.stream.Stream;

import smart_door.common.Context;

public class PersistentLogger {

    public PersistentLogger() {
        try {
            new File(Context.LOG_DB_FILE).createNewFile();
        } catch (Exception e) {
            System.err.println("Can't create log file. Error: " + e.getMessage());
        }
    }

    public Stream<String> getLogStream() {
        try {
            return Files.lines(Paths.get(Context.LOG_DB_FILE));
        } catch (Exception e) {
            System.err.println("Can't read log file. Error: " + e.getMessage());
            return null;
        }
    }

    public boolean log(String user, String event) {
        final LocalDateTime date = LocalDateTime.now();
        final String content = date.format(Context.DATE_FORMATTER) +
                Context.SEPARATOR +
                date.format(Context.TIME_FORMATTER) +
                Context.SEPARATOR +
                user +
                Context.SEPARATOR +
                event +
                "\n";
        try {
            Files.write(Paths.get(Context.LOG_DB_FILE), content.getBytes(), StandardOpenOption.APPEND);
            return true;
        } catch (Exception e) {
            System.err.println("Can't append log entry. Error: " + e.getMessage());
            return false;
        }
    }
}
