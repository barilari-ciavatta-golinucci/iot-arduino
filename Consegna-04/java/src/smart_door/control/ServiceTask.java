package smart_door.control;

import smart_door.common.Context;
import smart_door.events.Event;
import smart_door.events.MessageReceivedEvent;

public class ServiceTask extends EventLoopController {
    private Context context;

    public ServiceTask(Context context) {
        super();
        this.context = context;
    }

    @Override
    protected void processEvent(Event event) {
        if (event instanceof MessageReceivedEvent) {
            String message = ((MessageReceivedEvent) event).getMessage();
            if (message.startsWith(Context.MSG_RECEIVE_VALUES)) {
                message = message.substring(Context.MSG_RECEIVE_VALUES.length());
                String[] parts = message.split(Context.SEPARATOR);

                this.context.setTemperature(Integer.parseInt(parts[0]));
                this.context.setLightIntensity(Integer.parseInt(parts[1]));
            } else if (message.startsWith("DEBUG [ERROR]: ")) {
                this.context.getTerminal().errorln(message);
            } else if (message.startsWith("DEBUG [WARNING]: ") || message.startsWith("DEBUG [INFO]: ")) {
                this.context.getTerminal().outputln(message);
            }
        }
    }
}
