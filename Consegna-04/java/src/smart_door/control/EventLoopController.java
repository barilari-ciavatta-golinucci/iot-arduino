package smart_door.control;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import smart_door.events.Event;

/**
 * Defines the event loop control through a blocking event queue.
 */
public abstract class EventLoopController extends Thread {
    private static final int EVENT_QUEUE_MAX_SIZE = 200;
    private static final int DEFAULT_EVENT_QUEUE_SIZE = 50;

    private BlockingQueue<Event> eventQueue;

    public EventLoopController(int size) {
        if (size > 0 && size <= EVENT_QUEUE_MAX_SIZE) {
            this.eventQueue = new ArrayBlockingQueue<>(size);
        } else {
            this.eventQueue = new ArrayBlockingQueue<>(DEFAULT_EVENT_QUEUE_SIZE);
        }
    }

    public EventLoopController() {
        this(DEFAULT_EVENT_QUEUE_SIZE);
    }

    abstract protected void processEvent(Event event);

    public void run() {
        while (true) {
            try {
                Event event = this.waitForNextEvent();
                this.processEvent(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Event waitForNextEvent() throws InterruptedException {
        return this.eventQueue.take();
    }

    public boolean notifyEvent(Event event) {
        return this.eventQueue.offer(event);
    }
}
