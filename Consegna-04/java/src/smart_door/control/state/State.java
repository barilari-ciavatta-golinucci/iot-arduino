package smart_door.control.state;

import smart_door.events.Event;

public interface State {
	/**
	 * Run the state task
	 */
	void toDo(Event event);
}
