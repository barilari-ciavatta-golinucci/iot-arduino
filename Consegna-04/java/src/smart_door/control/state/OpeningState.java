package smart_door.control.state;

import smart_door.common.Context;
import smart_door.events.Event;
import smart_door.events.MessageReceivedEvent;

public class OpeningState implements State {
	private final StateController controller;
	private final Context context;
	
	public OpeningState(StateController controller, Context context) {
		this.controller = controller;
		this.context = context;
	}

	@Override
	public void toDo(Event event) {
		if (event instanceof MessageReceivedEvent) {
			String message = ((MessageReceivedEvent) event).getMessage();			
			if (message.equals(Context.MSG_OPEN)) {
				this.context.getTerminal().outputln("The door is opening");
				this.context.getLogger().log(this.context.getCurrentUser(), "DOOR_OPENING");
				this.controller.setState(new OpenState(controller, context));
			} else if (message.equals(Context.MSG_TIMEOUT)) {
				this.context.getTerminal().outputln("Time out. Access has not been done");
				this.context.getLogger().log(this.context.getCurrentUser(), "TIMEOUT");
				this.context.getLed(Context.LED_FAILED_ACCESS).blink(Context.FLASH_DELAY, Context.FLASH_DURATION);
				this.controller.setState(new WaitLoginState(controller, context));
			}
		}
	}
}
