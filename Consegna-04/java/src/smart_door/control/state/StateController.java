package smart_door.control.state;

import smart_door.common.Context;
import smart_door.events.Event;

public class StateController {
	private State state;
	
	public StateController(Context config){
		this.setState(new WaitLoginState(this, config));
	}
	
	public void setState(final State nextState) {
		this.state = nextState;
	}
	
	public void toDo(Event event) {
		this.state.toDo(event);
	}
}
