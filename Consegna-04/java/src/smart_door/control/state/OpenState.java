package smart_door.control.state;

import smart_door.common.Context;
import smart_door.events.Event;
import smart_door.events.MessageReceivedEvent;

public class OpenState implements State {
	private final StateController controller;
	private final Context context;
	
	public OpenState(StateController controller, Context context) {
		this.controller = controller;
		this.context = context;

		context.getLed(Context.LED_INSIDE).switchOn();
	}

	@Override
	public void toDo(Event event) {
		if (event instanceof MessageReceivedEvent) {
			String message = ((MessageReceivedEvent) event).getMessage();
			if (message.equals(Context.MSG_END)) {
				this.context.getTerminal().outputln("The user has closed the door");
				this.context.getLogger().log(this.context.getCurrentUser(), "DOOR_CLOSED");
				this.controller.setState(new WaitLoginState(controller, context));
			}
		}
	}
}
