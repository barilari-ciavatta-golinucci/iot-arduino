package smart_door.control.state;

import smart_door.common.Context;
import smart_door.events.Event;
import smart_door.events.MessageReceivedEvent;

public class WaitLoginState implements State {
	private final StateController controller;
	private final Context context;
	
	public WaitLoginState(StateController controller, Context context) {
		this.controller = controller;
		this.context = context;
		
		context.getLed(Context.LED_INSIDE).switchOff();
	}

	@Override
	public void toDo(Event event) {
		if (event instanceof MessageReceivedEvent) {
			String message = ((MessageReceivedEvent) event).getMessage();
			if (message.startsWith(Context.MSG_LOGIN)) {
				message = message.substring(Context.MSG_LOGIN.length());
				String[] parts = message.split(Context.SEPARATOR);
				if (this.context.checkLogin(parts[0], parts[1])) {
					this.context.setCurrentUser(parts[0]);
					this.context.getTerminal().outputln("Access granted for user " + parts[0]);
					this.context.getLogger().log(parts[0], "ACCESS_GRANTED");
					this.context.getSerial().sendMessage(Context.MSG_LOGIN_OK);
					this.controller.setState(new OpeningState(controller, context));
				} else {
					this.context.getTerminal().outputln("Access denied for user " + parts[0]);
					this.context.getLogger().log(parts[0], "ACCESS_DENIED");
					this.context.getLed(Context.LED_FAILED_ACCESS).blink(Context.FLASH_DELAY, Context.FLASH_DURATION);
					this.context.getSerial().sendMessage(Context.MSG_LOGIN_KO);
				}
			}			
		}
	}
}
