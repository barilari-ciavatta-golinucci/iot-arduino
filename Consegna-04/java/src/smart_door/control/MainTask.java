package smart_door.control;

import smart_door.control.state.StateController;
import smart_door.events.Event;

/**
 * Manages the Finite State Machine by checking states and deciding what to do.
 */
public class MainTask extends EventLoopController {
    private StateController context;

    public MainTask(StateController context) {
        super();
        this.context = context;
    }

    @Override
    protected void processEvent(Event event) {
        this.context.toDo(event);
    }
}
