package smart_door.server;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.IStatus;
import smart_door.common.Context;

public class WebService extends NanoHTTPD {

    /**
     * Some HTTP response status codes
     */
    private static final String HTTP_OK = "200 OK";
    private static final String HTTP_NOTFOUND = "404 Not Found";

    /**
     * Common mime types for dynamic content
     */
    private static final String MIME_PLAINTEXT = "text/plain";
    
    private static final Pattern REMOVE_LAST_COMMA_PATTERN = Pattern.compile("\\},\\n(\\s*)\\]");

    private final Context context;

    public WebService(Context context) {
        super(8080);
        this.context = context;

        try {
            start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
            context.getTerminal().outputln("WebService running at address http://localhost:8080/api");
        } catch (Exception e) {
            context.getTerminal().errorln("WebService failed starting. Error: " + e.getMessage());
        }
    }

    @Override
    public Response serve(IHTTPSession session) {
        Response response;
        if (session.getUri().equals("/api")) {
            response = buildJsonResponse(getApiList());
        } else if (session.getUri().equals("/api/logs")) {
            response = buildJsonResponse(computeLogsJson(session.getParms().get("date")));
        } else if (session.getUri().equals("/api/values")) {
            response = buildJsonResponse(computeValuesJson());
        } else {
            response = buildNotFoundErrorResponse();
        }

        this.context.getTerminal().outputln("Request url: " + session.getUri() + ", Response status: "
                + response.getStatus().getRequestStatus() + " (" + response.getStatus().getDescription() + ")");

        return response;
    }

    private String getApiList() {
        return "{\n" +
               "    \"api\": {\n" +
               "        \"logs\": \"Inspect the access log performed during the day\",\n" +
               "        \"values\": \"Read the current value of temperature and light intensity\"\n" +
               "    }\n" +
               "}";
    }

    private String computeLogsJson(String paramDate) {
        final StringBuilder sb = new StringBuilder();
        final String date = paramDate != null ? paramDate : LocalDate.now().format(Context.DATE_FORMATTER);

        sb.append("[\n");
        final Stream<String> logs = this.context.getLogger().getLogStream();
        if (logs != null) {
            logs.filter(log -> log.startsWith(date))
                .map(log -> Arrays.asList(log.split(Context.SEPARATOR))).collect(
                        Collectors.groupingBy(log -> log.get(2),
                                Collectors.mapping(Function.identity(), Collectors.toSet())))                
                .forEach((username, set) -> {
                    sb.append("    {\n");
                    sb.append("        \"" + username + "\": [\n");
                    set.forEach(list -> {
                            sb.append("            {\n");
                            sb.append("                \"time\": \"" + list.get(1) + "\",\n");
                            sb.append("                \"event\": \"" + list.get(3) + "\"\n");
                            sb.append("            },\n");
                    });
                    sb.append("        ]\n");
                    sb.append("    },\n");
                });
        }
        sb.append("]");
        
        final Matcher m = REMOVE_LAST_COMMA_PATTERN.matcher(sb.toString());
        return m.replaceAll("}\n$1]");
    }

    private String computeValuesJson() {
        return "{\n" +
               "    \"values\": {\n" +
               "        \"temperature\": " + this.context.getTemperature() + ",\n" +
               "        \"lightIntensity\": " + this.context.getLightIntensity() + "\n" +
               "    }\n" +
               "}";
    }

    private Response buildJsonResponse(String message) {
        return newFixedLengthResponse(new IStatus() {
            @Override
            public int getRequestStatus() {
                return 200;
            }

            @Override
            public String getDescription() {
                return HTTP_OK;
            }
        }, MIME_PLAINTEXT, message);
    }

    private Response buildNotFoundErrorResponse() {
        return newFixedLengthResponse(new IStatus() {
            @Override
            public int getRequestStatus() {
                return 404;
            }

            @Override
            public String getDescription() {
                return HTTP_NOTFOUND;
            }
        }, MIME_PLAINTEXT, HTTP_NOTFOUND);
    }
}
