package smart_door.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import smart_door.control.EventLoopController;
import smart_door.control.MainTask;
import smart_door.control.ServiceTask;
import smart_door.control.state.StateController;
import smart_door.devices.Led;
import smart_door.devices.PersistentLogger;
import smart_door.devices.Serial;
import smart_door.devices.Terminal;
import smart_door.events.MessageReceivedEvent;
import smart_door.events.MessageReceivedListener;
import smart_door.server.WebService;
import smart_door.utils.SerialUtils;

/**
 * Class which starts the program.
 */
public class SmartDoor {
    private final Terminal terminal = new Terminal();
    private final PersistentLogger logger = new PersistentLogger();
    private Serial serial;

    public static void main(String[] args) {
        SmartDoor smartDoor = new SmartDoor();
        smartDoor.welcome();
        String port = smartDoor.choosePortAndGet();
        if (smartDoor.initConnection(port)) {
            smartDoor.terminal.outputln("Connected to the door. Press CTRL + C to exit");
            smartDoor.start();
        } else {
            smartDoor.terminal.errorln("Error: can't connect to the door.");
        }
    }

    private void start() {
        final Map<String, Led> leds = new HashMap<>();
        leds.put(Context.LED_INSIDE, new Led(2, Context.LED_INSIDE));
        leds.put(Context.LED_FAILED_ACCESS, new Led(0, Context.LED_FAILED_ACCESS));

        final Context context = new Context(leds, this.serial, this.terminal, this.logger);
        final EventLoopController mainTask = new MainTask(new StateController(context));
        mainTask.start();

        final EventLoopController serviceTask = new ServiceTask(context);
        serviceTask.start();

        this.serial.addMessageReceivedListener(new MessageReceivedListener() {
            @Override
            public void onMessageReceived(String message) {
                final MessageReceivedEvent mre = new MessageReceivedEvent(message);
                mainTask.notifyEvent(mre);
                serviceTask.notifyEvent(mre);
            }
        });

        new WebService(context);

        this.clearResourcesOnClose();
    }

    private void welcome() {
        this.terminal.outputln("<< Welcome to the Smart Door >>");
    }

    /**
     * 
     * @return the serial port of Arduino
     */
    private String choosePortAndGet() {
        String question = "Choose the serial port of the door";
        List<String> answers = SerialUtils.getAvailableSerialPorts();

        return this.terminal.makeQuestionAndGetAnswer(question, answers);
    }

    /**
     * 
     * @param serialPort
     *            the serial port of Arduino
     * @return true if a connection has been established
     */
    private boolean initConnection(final String serialPort) {
        try {
            this.serial = new Serial(serialPort, 9600);
        } catch (Exception e) {
            terminal.errorln("An error occurred: " + e.getMessage());
            return false;
        }

        this.terminal.output("Waiting for setting up the connection with the door");
        this.terminal.sleepMilliseconds(4000);
        return true;
    }

    private void clearResourcesOnClose() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                terminal.outputln("Connection to the door closed. Bye");
            }
        });
    }
}
