package smart_door.common;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import smart_door.devices.Led;
import smart_door.devices.PersistentLogger;
import smart_door.devices.Serial;
import smart_door.devices.Terminal;

/**
 * A context shared class.
 */
public class Context {
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("hh:mm:ss a", Locale.ENGLISH);
    public static final String LED_INSIDE = "LED_INSIDE";
    public static final String LED_FAILED_ACCESS = "LED_FAILED_ACCESS";
    public static final int FLASH_DELAY = 1000;
    public static final int FLASH_DURATION = 1000;
    public static final String SEPARATOR = ";";
    public static final String MSG_LOGIN_OK = "LOGIN_OK";
    public static final String MSG_LOGIN_KO = "LOGIN_KO";
    public static final String MSG_OPEN = "OPEN";
    public static final String MSG_TIMEOUT = "TIMEOUT";
    public static final String MSG_END = "END";
    public static final String MSG_LOGIN = "LOGIN+";
    public static final String MSG_RECEIVE_VALUES = "VALUES+";
    public static final String LOG_DB_FILE = "log.db";

    private final Map<String, Led> leds;
    private final Serial serial;
    private final Terminal terminal;
    private final PersistentLogger logger;
    private final Map<String, String> users;
    private String currentUser;
    private int temperature;
    private int lightIntensity;

    public Context(Map<String, Led> leds, Serial serial, Terminal terminal, PersistentLogger logger) {
        this.leds = leds;
        this.serial = serial;
        this.terminal = terminal;
        this.logger = logger;

        users = new HashMap<>();
        users.put("emiliano", "ciavatta");
        users.put("nicolas", "barilari");
        users.put("simone", "golinucci");
    }

    public Led getLed(String ledName) {
        return this.leds.get(ledName);
    }

    public Serial getSerial() {
        return this.serial;
    }

    public Terminal getTerminal() {
        return this.terminal;
    }

    public PersistentLogger getLogger() {
        return this.logger;
    }

    public boolean checkLogin(final String username, final String password) {
        return users.containsKey(username) && users.get(username).equals(password);
    }

    public String getCurrentUser() {
        return this.currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public int getTemperature() {
        return this.temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getLightIntensity() {
        return this.lightIntensity;
    }

    public void setLightIntensity(int lightIntensity) {
        this.lightIntensity = lightIntensity;
    }
}
