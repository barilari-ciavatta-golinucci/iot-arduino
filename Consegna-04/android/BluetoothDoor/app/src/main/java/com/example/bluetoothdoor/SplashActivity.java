package com.example.bluetoothdoor;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import com.example.bluetoothdoor.util.Constants;

public class SplashActivity extends AppCompatActivity implements Runnable {
    private Handler mHandler = null;
    private RotateAnimation mAnim = null;
    private ImageView mSplashIcon = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mSplashIcon = findViewById(R.id.splash_icon);
        mHandler = new Handler();
        mAnim = new RotateAnimation(0f, 365f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mAnim.setInterpolator(new LinearInterpolator());
        mAnim.setRepeatCount(0);
        mAnim.setDuration(Constants.SPLASH_SCREEN_ANIMATION_DURATION);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Add runnable to the message queue
        mHandler.postDelayed(this, Constants.SPLASH_SCREEN_DURATION);
        mSplashIcon.startAnimation(mAnim);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Remove all runnables from the message queue
        mHandler.removeCallbacks(this);
    }

    @Override
    public void run() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
