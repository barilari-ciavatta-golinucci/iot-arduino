package com.example.bluetoothdoor;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class AuthActivity extends AppCompatActivity {
    private String mMessageReceived = "";
    private EditText mUsername = null;
    private EditText mPassword = null;
    private LinearLayout mProgressBar = null;
    private Bluetooth mBluetooth = null;
    private boolean mIsOpening = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.action_bar_auth);

        ConnectionThread connection = ConnectionThread.getInstance(false);
        connection.setContext(this);

        mUsername = findViewById(R.id.username_edit_text);
        mPassword = findViewById(R.id.password_edit_text);
        mProgressBar = findViewById(R.id.progress_bar2_layout);
    }

    public void connectionFailed(Message msg) {
        String str = msg.getData().getString("Failure");
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void sendMessage(View view) {
        if (mBluetooth != null) {
            String str = "LOGIN+" + mUsername.getText().toString() + ";" + mPassword.getText().toString();
            mBluetooth.sendMessage(str);
        }
    }

    public void messageRead(Message msg) {
        String str = msg.getData().getString("Read");
        if (!str.contains("\n")) {
            mMessageReceived = mMessageReceived.concat(str);
            return;
        } else {
            str = mMessageReceived.concat(str);
            mMessageReceived = "";
        }
        str = str.replaceAll("[\n\r]", "");
        Intent intent;
        if (!str.equals("")) {
            switch (str) {
                case ("WRONG_LOGIN") :
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.warning)
                            .setMessage("Wrong credentials")
                            .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    mUsername.setText("");
                                    mPassword.setText("");
                                    dialogInterface.dismiss();
                                }
                            })
                            .setCancelable(false)
                            .create()
                            .show();
                    break;
                case ("OK"):
                    Toast.makeText(this, "Correct credentials", Toast.LENGTH_SHORT).show();
                    mIsOpening = true;
                    mProgressBar.setVisibility(View.VISIBLE);
                    break;
                case ("OPEN"):
                    if (mIsOpening) {
                        mIsOpening = false;
                        Toast.makeText(this, "Door open!", Toast.LENGTH_LONG).show();
                        intent = new Intent(this, ControlUnitActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    break;
                case ("TOO_ATTEMPTS"):
                    Toast.makeText(this, "Too attempts!", Toast.LENGTH_LONG).show();
                    intent = new Intent(this, AuthActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                case ("CLOSING"):
                    if (mIsOpening) {
                        mIsOpening = false;
                        Toast.makeText(this, "Timeout, session closed.", Toast.LENGTH_LONG).show();
                        mBluetooth.cancel();
                        intent = new Intent(this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    break;
            }
        }
    }

    public void setBluetooth(Bluetooth bluetooth) {
        mBluetooth = bluetooth;
    }

}
