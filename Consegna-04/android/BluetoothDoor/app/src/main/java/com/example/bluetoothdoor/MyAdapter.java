package com.example.bluetoothdoor;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.bluetoothdoor.util.Constants;
import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private List<Device> mDeviceList = new ArrayList<>();
    private Context mContext;
    private ListItemClickListener mListItemClickListener;
    private boolean mAnimation;

    // Package private
    MyAdapter(Context context, List<Device> deviceList, ListItemClickListener listener) {
        mContext = context;
        mDeviceList = deviceList;
        mListItemClickListener = listener;
        mAnimation = false;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {

        private ImageView mDeviceIcon;
        private TextView mDeviceName;
        private TextView mDeviceState;

        MyViewHolder(View itemView) {
            super(itemView);
            mDeviceIcon = itemView.findViewById(R.id.device_icon);
            mDeviceName = itemView.findViewById(R.id.device_name);
            mDeviceState = itemView.findViewById(R.id.device_state);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            setAnimation(view);
            mListItemClickListener.onListItemClick(new ArrayList<>(mDeviceList), getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            mListItemClickListener.onListItemLongClick(new ArrayList<>(mDeviceList), getAdapterPosition());
            return true;
        }
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.mDeviceIcon.setImageResource(R.drawable.device_icon);
        holder.mDeviceName.setText(mDeviceList.get(position).getName());
        holder.mDeviceState.setText(mDeviceList.get(position).getState());
        if (mAnimation) {
            setAnimation(holder.itemView);
        }
    }

    @Override
    public int getItemCount() {
        return mDeviceList.size();
    }

    private void setAnimation(final View viewToAnimate) {
        int colorFrom = ContextCompat.getColor(viewToAnimate.getContext(), R.color.colorAccent);
        int colorTo = Color.WHITE;

        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(Constants.ANIMATION_DURATION);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                viewToAnimate.setBackgroundColor((int) animator.getAnimatedValue());
            }
        });
        colorAnimation.start();
    }

    public void setAnimation(boolean bool) {
        mAnimation = bool;
    }

    public interface ListItemClickListener {
        void onListItemClick(List<Device> deviceList, int position);
        void onListItemLongClick(List<Device> deviceList, int position);
    }
}
