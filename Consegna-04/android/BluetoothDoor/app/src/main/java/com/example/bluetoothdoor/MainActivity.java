package com.example.bluetoothdoor;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.example.bluetoothdoor.async.BluetoothDiscoverAsync;
import com.example.bluetoothdoor.util.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity
        implements MyAdapter.ListItemClickListener,
        BluetoothDiscoverAsync.Listener {

    private final BluetoothDiscoverAsync mBtDiscoverAsync = new BluetoothDiscoverAsync(this);
    private BluetoothAdapter mBluetoothAdapter = null;
    private List<Device> mPairedDevicesList = new ArrayList<>();
    private List<Device> mAvailableDevicesList = new ArrayList<>();
    private RecyclerView mPairedDevicesRecyclerView = null;
    private RecyclerView mAvailableDevicesRecyclerView = null;
    private MyAdapter mPairedDevicesAdapter = null;
    private MyAdapter mAvailableDevicesAdapter = null;
    private ToneGenerator mTone = null;
    private ConnectionThread mConnection = null;
    private String mMessageReceived = "";

    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BluetoothDevice.ACTION_FOUND.equals(intent.getAction())) {
                // Discovery has found a device. Get the BluetoothDevice object and its info from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getName() == null) {
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                }
                if (device.getName() != null && !device.getName().isEmpty() && !device.getName().startsWith(" ")) {
                    Device newDevice = new Device(device.getName(), device.getAddress(), getResources().getString(R.string.found_device_label));
                    List<String> addresses = getAddressList(mAvailableDevicesList);
                    if (!addresses.contains(newDevice.getAddress())) {
                        mAvailableDevicesList.add(newDevice);
                        int position = mAvailableDevicesList.size() - 1;
                        mAvailableDevicesAdapter.notifyItemInserted(position);
                        mAvailableDevicesRecyclerView.scrollToPosition(position);
                        mTone.startTone(ToneGenerator.TONE_CDMA_ALERT_INCALL_LITE, Constants.SOUND_DURATION);
                    }
                    addresses = getAddressList(mPairedDevicesList);
                    newDevice = new Device(device.getName(), device.getAddress(), getResources().getString(R.string.paired_device_label));
                    if (!addresses.contains(newDevice.getAddress())) {
                        mPairedDevicesList.add(newDevice);
                        int position = mPairedDevicesList.size() - 1;
                        mPairedDevicesAdapter.notifyItemInserted(position);
                        mPairedDevicesRecyclerView.scrollToPosition(position);
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.app_name);

        RecyclerView.LayoutManager pairedDevicesLayoutManager = new LinearLayoutManager(this);
        mPairedDevicesRecyclerView = findViewById(R.id.paired_devices_recycler_view);
        mPairedDevicesRecyclerView.setLayoutManager(pairedDevicesLayoutManager);
        mPairedDevicesRecyclerView.setHasFixedSize(true);
        mPairedDevicesAdapter = new MyAdapter(this, mPairedDevicesList, this);
        mPairedDevicesRecyclerView.setAdapter(mPairedDevicesAdapter);

        RecyclerView.LayoutManager availableDevicesLayoutManager = new LinearLayoutManager(this);
        mAvailableDevicesRecyclerView = findViewById(R.id.available_devices_recycler_view);
        mAvailableDevicesRecyclerView.setLayoutManager(availableDevicesLayoutManager);
        mAvailableDevicesRecyclerView.setHasFixedSize(true);
        mAvailableDevicesAdapter = new MyAdapter(this, mAvailableDevicesList, this);
        mAvailableDevicesAdapter.setAnimation(true);
        mAvailableDevicesRecyclerView.setAdapter(mAvailableDevicesAdapter);

        String permission = Manifest.permission.ACCESS_FINE_LOCATION;
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{permission}, Constants.REQUEST_CODE_PERMISSION_INITIAL);
        }

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (this.bluetoothInit()) {
            this.enableBluetooth();
        }

        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter);

        mTone = new ToneGenerator(AudioManager.STREAM_MUSIC, Constants.SOUND_VOLUME);

        mConnection = ConnectionThread.getInstance(true);
        mConnection.setContext(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBluetoothAdapter.isEnabled()) {
            this.queryPairDevices();

            if (!mBluetoothAdapter.isDiscovering() && mBtDiscoverAsync.getStatus() == AsyncTask.Status.PENDING) {
                mBtDiscoverAsync.execute(mBluetoothAdapter);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBluetoothAdapter.cancelDiscovery();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Unregister the ACTION_FOUND receiver
        unregisterReceiver(mReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.bt_search_icon) {
            if (mBluetoothAdapter.isEnabled()) {
                if (!mBluetoothAdapter.isDiscovering() && mBtDiscoverAsync.getStatus() != AsyncTask.Status.RUNNING) {
                    new BluetoothDiscoverAsync(this).execute(mBluetoothAdapter);
                }
            } else {
                this.enableBluetooth();
            }
        }
        return true;
    }

    @Override
    public void onListItemClick(final List<Device> deviceList, final int position) {
        new AlertDialog.Builder(this)
                .setTitle(deviceList.get(position).getName())
                .setMessage(getString(R.string.mac_address) + " " + deviceList.get(position).getAddress())
                .setPositiveButton(R.string.dialog_connect, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        findViewById(R.id.bt_search_icon).setVisibility(View.INVISIBLE);
                        findViewById(R.id.progress_bar_layout).setVisibility(View.VISIBLE);
                        mConnection.setDeviceAddress(deviceList.get(position).getAddress());
                        mConnection.run();
                    }
                })
                .setNegativeButton(R.string.dialog_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    @Override
    public void onListItemLongClick(final List<Device> deviceList, final int position) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.warning)
                .setMessage(String.format(getString(R.string.delete_device), deviceList.get(position).getName()))
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        for (Device device : mPairedDevicesList) {
                            if (device.getAddress().equals(deviceList.get(position).getAddress())) {
                                mPairedDevicesList.remove(device);
                                mPairedDevicesAdapter.notifyDataSetChanged();
                                break;
                            }
                        }

                        for (Device device : mAvailableDevicesList) {
                            if (device.getAddress().equals(deviceList.get(position).getAddress())) {
                                mAvailableDevicesList.remove(device);
                                mAvailableDevicesAdapter.notifyDataSetChanged();
                                break;
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.REQUEST_CODE_PERMISSION_INITIAL) {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.error)
                        .setMessage("Fine location permission required to run this app!")
                        .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                finish();
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            }
        }
    }

    private boolean bluetoothInit() {
        if (mBluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            Log.e("MainActivity", "Bluetooth is not available on this device.");

            new AlertDialog.Builder(this)
                    .setTitle(R.string.error)
                    .setMessage(R.string.no_bluetooth)
                    .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();

            return false;
        }
        return true;
    }

    private void enableBluetooth() {
        if (!mBluetoothAdapter.isEnabled()) {
            // Bluetooth is not currently enabled
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BLUETOOTH);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_ENABLE_BLUETOOTH && resultCode == AppCompatActivity.RESULT_OK) {
            Toast.makeText(this, R.string.toast_active_bt, Toast.LENGTH_LONG).show();
            if (!mBluetoothAdapter.isDiscovering() && mBtDiscoverAsync.getStatus() != AsyncTask.Status.RUNNING) {
                new BluetoothDiscoverAsync(MainActivity.this).execute(mBluetoothAdapter);
            }
        } else {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.warning)
                    .setMessage(R.string.no_bluetooth_activation)
                    .setPositiveButton(R.string.dialog_activate, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            MainActivity.this.enableBluetooth();
                            dialogInterface.dismiss();
                        }
                    })
                    .setNegativeButton(R.string.dialog_close, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    private void queryPairDevices() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            List<String> addresses = getAddressList(mPairedDevicesList);

            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                if (!addresses.contains(device.getAddress())) {
                    String deviceName = device.getName();
                    String deviceAddress = device.getAddress(); // MAC address
                    String deviceState = getResources().getString(R.string.paired_device_label);
                    mPairedDevicesList.add(new Device(deviceName, deviceAddress, deviceState));
                    int position = mPairedDevicesList.size() - 1;
                    mPairedDevicesAdapter.notifyItemInserted(position);
                    mPairedDevicesRecyclerView.scrollToPosition(position);
                }
            }
        }
    }

    private List<String> getAddressList(List<Device> deviceList) {
        List<String> addresses = new ArrayList<>();
        for (Device device : deviceList) {
            addresses.add(device.getAddress());
        }
        return addresses;
    }

    public void connectionFailed(Message msg) {
        findViewById(R.id.progress_bar_layout).setVisibility(View.GONE);
        findViewById(R.id.bt_search_icon).setVisibility(View.VISIBLE);
        String str = msg.getData().getString("Failure");
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }

    public void messageRead(Message msg) {
        String str = msg.getData().getString("Read");
        if (!str.contains("\n")) {
            mMessageReceived = mMessageReceived.concat(str);
            return;
        } else {
            str = mMessageReceived.concat(str);
            mMessageReceived = "";
        }
        str = str.replaceAll("[\r\n]", "");
        if (!str.equals("") && str.equals("HELLO")) {
            Intent intent = new Intent(this, AuthActivity.class);
            Toast.makeText(this, "Connection successful!", Toast.LENGTH_LONG).show();
            startActivity(intent);
            finish();
        }
    }
}
