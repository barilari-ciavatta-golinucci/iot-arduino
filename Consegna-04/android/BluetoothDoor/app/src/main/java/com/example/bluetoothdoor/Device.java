package com.example.bluetoothdoor;

public class Device {
    private String mName;
    private String mAddress; // MAC address
    private String mState;

    public Device(String name, String address, String state) {
        mName = name;
        mAddress = address;
        mState = state;
    }

    public String getName() {
        return mName;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getState() {
        return mState;
    }
}
