package com.example.bluetoothdoor.util;

public class Constants {

    // bluetooth
    public static final int REQUEST_ENABLE_BLUETOOTH = 123;
    public static final String UUID = "00001101-0000-1000-8000-00805F9B34FB";

    // splash screen
    public static final int SPLASH_SCREEN_DURATION = 2700; // milliseconds
    public static final int SPLASH_SCREEN_ANIMATION_DURATION = 1000; // milliseconds

    // animation
    public static final int ANIMATION_DURATION = 600; // milliseconds

    // sound
    public static final int SOUND_VOLUME = 100; // from 0 to 100
    public static final int SOUND_DURATION = 100; // milliseconds

    // privacy permission
    public static int REQUEST_CODE_PERMISSION_INITIAL = 456;
}
