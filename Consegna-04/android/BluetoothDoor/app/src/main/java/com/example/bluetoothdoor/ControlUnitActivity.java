package com.example.bluetoothdoor;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class ControlUnitActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {
    private Bluetooth mBluetooth = null;
    private SeekBar mSeekBar = null;
    private TextView mTemperature = null;
    private String mMessageReceived = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_unit);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Control Unit");

        ConnectionThread connection = ConnectionThread.getInstance(false);
        connection.setContext(this);

        mSeekBar = findViewById(R.id.seekBar);
        mSeekBar.setOnSeekBarChangeListener(this);
        mTemperature = findViewById(R.id.temp_text);
    }

    public void setBluetooth(Bluetooth bluetooth) {
        mBluetooth = bluetooth;
    }

    public void endSession(View view) {
        if (mBluetooth != null) {
            mBluetooth.sendMessage("END");
        }
        Toast.makeText(this, "Session ended!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void messageRead(Message msg) {
        String str = msg.getData().getString("Read");
        if (!str.contains("\n")) {
            mMessageReceived = mMessageReceived.concat(str);
            return;
        } else {
            str = mMessageReceived.concat(str);
            mMessageReceived = "";
        }
        str = str.replaceAll("[\r\n]", "");
        if (str.startsWith("TEMP+")) {
            mTemperature.setText("Temperature is " + str.substring(str.indexOf("+") + 1));
        } else if (str.equals("CLOSE")) {
            this.endSession(null);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser && progress >= 0 && progress <= 100) {
            mBluetooth.sendMessage("VALUE+" + progress);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) { }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) { }
}
