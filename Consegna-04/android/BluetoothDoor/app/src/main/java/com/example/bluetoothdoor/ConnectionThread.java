package com.example.bluetoothdoor;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class ConnectionThread extends Thread {
    private static ConnectionThread mInstance = null;
    private Context mContext = null;
    private String mDeviceAddress = null;
    private Bluetooth mBluetooth = null;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bluetooth.MESSAGE_READ:
                    if (mContext instanceof MainActivity) {
                        ((MainActivity) mContext).messageRead(msg);
                    } else if (mContext instanceof AuthActivity) {
                        ((AuthActivity) mContext).messageRead(msg);
                    } else if (mContext instanceof ControlUnitActivity) {
                        ((ControlUnitActivity) mContext).messageRead(msg);
                    }
                    break;
                case Bluetooth.MESSAGE_WRITE:
                    break;
                case Bluetooth.MESSAGE_FAILED:
                    if (mContext instanceof MainActivity) {
                        ((MainActivity) mContext).connectionFailed(msg);
                    } else if (mContext instanceof AuthActivity) {
                        ((AuthActivity) mContext).connectionFailed(msg);
                    }
                    break;
            }
        }
    };

    private ConnectionThread() {
    }

    public static ConnectionThread getInstance(boolean newObj) {
        if (mInstance == null || newObj) {
            mInstance = new ConnectionThread();
        }
        return mInstance;
    }

    @Override
    public void run() {
        boolean success = true;
         mBluetooth = new Bluetooth(mHandler);
        try {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (bluetoothAdapter.isEnabled()) {
                mBluetooth.connectDevice(mDeviceAddress);
            } else {
                success = false;
            }
        } catch(Exception e){
            e.printStackTrace();
            success = false;
        }

        if (!success) {
            Bundle bundle = new Bundle();
            bundle.putString("Failure", "Unable to connect device");
            Message msg = new Message();
            msg.setData(bundle);
            if (mContext instanceof MainActivity) {
                ((MainActivity) mContext).connectionFailed(msg);
            } else if (mContext instanceof AuthActivity) {
                ((AuthActivity) mContext).connectionFailed(msg);
            }
        }
    }

    public void setContext(Context context) {
        mContext = context;
        if (mContext instanceof AuthActivity) {
            ((AuthActivity) mContext).setBluetooth(mBluetooth);
        } else if (mContext instanceof ControlUnitActivity) {
            ((ControlUnitActivity) mContext).setBluetooth(mBluetooth);
        }
    }

    public void setDeviceAddress(String address) {
        mDeviceAddress = address;
    }

}
