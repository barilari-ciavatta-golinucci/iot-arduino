package com.example.bluetoothdoor.async;

import android.bluetooth.BluetoothAdapter;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import com.example.bluetoothdoor.MainActivity;
import com.example.bluetoothdoor.R;

public class BluetoothDiscoverAsync extends AsyncTask<BluetoothAdapter, Void, Boolean> {
    private Listener mListener;

    // The listener that will handle the task completion
    public BluetoothDiscoverAsync(Listener listener) {
        super();
        mListener = listener;
    }

    // Runs on UI thread
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mListener instanceof MainActivity) {
            ProgressBar bar = ((MainActivity)mListener).findViewById(R.id.progress_bar);
            bar.setVisibility(View.VISIBLE);
        }
    }

    // Runs on worker thread
    @Override
    protected Boolean doInBackground(BluetoothAdapter... args) {
        BluetoothAdapter bluetoothAdapter = args[0];
        if (bluetoothAdapter != null) {
            bluetoothAdapter.startDiscovery();
            try {
                Thread.sleep(12000); // milliseconds
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            bluetoothAdapter.cancelDiscovery();
            return true;
        }
        return false;
    }

    // Runs on UI thread
    @Override
    protected void onPostExecute(Boolean success) {
        super.onPostExecute(success);
        if (mListener instanceof MainActivity) {
            ProgressBar bar = ((MainActivity) mListener).findViewById(R.id.progress_bar);
            bar.setVisibility(View.INVISIBLE);
        }
        mListener = null;
    }

    public interface Listener {
    }
}
