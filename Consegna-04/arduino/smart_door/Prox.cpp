#include "Prox.h"

Prox::Prox(int trig, int echo) {
    this->trigPin = trig;
    this->echoPin = echo;
    pinMode(trigPin, OUTPUT);
    pinMode(echoPin, INPUT);
}

int Prox::getDistance() {
    long duration, cm;
    //emissione del suono
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    //ricezione del suono
    duration = pulseIn(echoPin, HIGH);
    //conversione del tempo in spazio
    cm = microsecondsToCentimeters(duration);
    return cm;
}

int Prox::microsecondsToCentimeters(long microseconds) {
    //la velocità del suono è 340 m/s o 29 millisecondi per centimetro.
    // nel calcolo viene diviso per 2 perché il la distanza rilevata è pari allo spazioe dell' andata e del ritorno del suono
    return microseconds / 29 / 2;
}
