#ifndef __TASK__
#define __TASK__

class Task {
    int period;
    int timeElapsed;

public:
    virtual void tick() = 0;

    virtual void init(int period) {
        this->period = period;
        this->timeElapsed = 0;
    }

    bool updateAndCheckTime(int basePeriod) {
        this->timeElapsed += basePeriod;
        if (this->timeElapsed >= period) {
            this->timeElapsed = 0;
            return true;
        } else {
            return false;
        }
    }
};

#endif
