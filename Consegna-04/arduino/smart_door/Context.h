#ifndef __CONTEXT__
#define __CONTEXT__

#include "Arduino.h"

#define MIN_DIST 50
#define MIN_SEC 3000
#define MAX_DELAY 10000
#define MAX_ATTEMPTS 5
#define SEPARATOR ";"

#define NOTHING 0
#define SAY_HELLO 1
#define CHECK_LOGIN 2
#define ACCESS_GRANTED 3
#define ACCESS_DENIED 4
#define MAX_ATTEMPTS_REACHED 5
#define PRESENCE_DETECTED 6
#define PRESENCE_UNDETECTED 7
#define COMMAND_CLOSE 8

class Context {
    public:
        static Context* getInstance();
        void setGlobal(int global);
        int getGlobal();
        void setTemperature(int temp);
        int getTemperature();
        void setLedIntensity(int intensity);
        int getLedIntensity();
        void setUsername(String usr);
        String getUsername();
        void setPassword(String passwd);
        String getPassword();

    private:
        Context();
        static Context *instance;
        int global;
        int temperature;
        int ledIntensity;
        String username;
        String password;
};

#endif
