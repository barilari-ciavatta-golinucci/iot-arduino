#ifndef __MAINTASK__
#define __MAINTASK__

#include "Arduino.h"
#include <Servo.h>
#include "Button.h"
#include "Context.h"
#include "Led.h"
#include "Log.h"
#include "Pir.h"
#include "Prox.h"
#include "Task.h"
#include "Temperature.h"

class MainTask: public Task {
    enum State {CLOSED, WAIT, LOGIN, OPENING, OPEN};

    public:
        MainTask();
        void init(int period);
        void tick();

    private:
        Context *context;
        Led *ledValue;
        Led *ledOn;
        Servo servoMotor;
        Pir *pir;
        Prox *prox;
        Temperature *temp;
        Button *btnExit;
        State state;
        unsigned int period;
};

#endif
