#ifndef __BLUETOOTH__
#define __BLUETOOTH__

#include "Arduino.h"
#include "SoftwareSerial.h"
#include "Log.h"

class Bluetooth {

public:
    Bluetooth(int rxPin, int txPin);
    void init();
    String receiveMessage();
    void sendMessage(String message);

private:
    SoftwareSerial* channel;
};

#endif
