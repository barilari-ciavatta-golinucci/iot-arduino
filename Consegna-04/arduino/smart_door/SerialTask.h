#ifndef __SERIALTASK__
#define __SERIALTASK__

#include "Arduino.h"
#include "Context.h"
#include "Log.h"
#include "Task.h"

class SerialTask: public Task {
    enum State {CLOSED, LOGIN, OPENING, OPEN};

    public:
        SerialTask();
        void init(int period);
        void tick();

    private:
        Context *context;
        State state;
        unsigned int period;

        void sendMessage(String msg);
};

#endif
