#include "Temperature.h"

Temperature::Temperature(int pin) {
    this->pin = pin;
    pinMode(this->pin, INPUT);
}

int Temperature::getTemperature() {
    return round(analogRead(pin) * 0.48875);
}
