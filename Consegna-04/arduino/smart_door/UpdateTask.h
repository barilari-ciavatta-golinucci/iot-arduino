#ifndef __UPDATETASK__
#define __UPDATETASK__

#include "Arduino.h"
#include "Context.h"
#include "Log.h"
#include "Task.h"

class UpdateTask: public Task {
    public:
        UpdateTask();
        void init(int period);
        void tick();

    private:
        Context *context;
        unsigned int period;
};

#endif
