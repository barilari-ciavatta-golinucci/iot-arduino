#include "Bluetooth.h"

Bluetooth::Bluetooth(int rxPin, int txPin) {
    channel = new SoftwareSerial(rxPin, txPin);
}

void Bluetooth::init() {
    channel->begin(9600);
}

void Bluetooth::sendMessage(String message) {
    channel->println(message);
    channel->flush();
}

String Bluetooth::receiveMessage() {
    String message = "";

    while (channel->available()) {
        message += (char) channel->read();
    }

    return message;
}



