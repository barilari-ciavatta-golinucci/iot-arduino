/**
 * Messaggi che vengono scritti nella seriale del Bluetooth:
 * CLOSE        : Inviato quando la porta viene chiusa
 * CLOSING      : Inviato quando non viene rilevata la presenza
 * HELLO        : Inviato quando bisogna salutare l'app mobile
 * OK           : Inviato quando le credenziali sono corrette
 * OPEN         : Inviato quando il pir rileva la presenza all'interno della stanza
 * TOO_ATTEMPTS : Inviato quando sono stati falliti troppi tentativi
 */

#include "BluetoothTask.h"

BluetoothTask::BluetoothTask() {
    context = Context::getInstance();
    bluetooth = new Bluetooth(10, 11);
}

void BluetoothTask::init(int period) {
    Task::init(period);
    this->period = period;
    state = WORKING;
    bluetooth->init();
}

void BluetoothTask::tick() {
    static unsigned short int attempts = 0;
    static unsigned int counter = 0;
    String message;

    switch (state) {
        case WORKING:
            if (context->getGlobal() == PRESENCE_UNDETECTED) {
                bluetooth->sendMessage("CLOSING");
            } else if (context->getGlobal() == SAY_HELLO) {
                attempts = 0;
                bluetooth->sendMessage("HELLO");
                state = LOGIN;
                Log::info("BluetoothTask: WORKING -> LOGIN");
            } else if (context->getGlobal() == PRESENCE_DETECTED) {
                bluetooth->sendMessage("OPEN");
                state = OPEN;
                Log::info("BluetoothTask: WORKING -> OPEN");
            }

            break;
        case LOGIN:
            message = bluetooth->receiveMessage();
            if (message.startsWith("LOGIN+")) {
                context->setUsername(message.substring(message.indexOf('+') + 1, message.indexOf(';')));
                context->setPassword(message.substring(message.indexOf(';') + 1));
                context->setGlobal(CHECK_LOGIN);
            } else if (context->getGlobal() == ACCESS_GRANTED) {
                bluetooth->sendMessage("OK");
                state = WORKING;
                Log::info("BluetoothTask: LOGIN -> WORKING");
            } else if (context->getGlobal() == ACCESS_DENIED) {
                bluetooth->sendMessage("WRONG_LOGIN");
                attempts++;
                context->setGlobal(SAY_HELLO);
            }

            if (attempts >= MAX_ATTEMPTS) {
                context->setGlobal(MAX_ATTEMPTS_REACHED);
                bluetooth->sendMessage("TOO_ATTEMPTS");
                state = WORKING;
                Log::info("BluetoothTask: LOGIN -> WORKING");
            }

            break;
        case OPEN:
            message = bluetooth->receiveMessage();
            if (message == "END") {
                context->setGlobal(COMMAND_CLOSE);
                state = WORKING;
                Log::info("BluetoothTask: OPEN -> WORKING");
            } else if (context->getGlobal() == COMMAND_CLOSE) {
                bluetooth->sendMessage("CLOSE");
                state = WORKING;
                Log::info("BluetoothTask: OPEN -> WORKING");
            } else if (message.startsWith("VALUE+")) {
                context->setLedIntensity(message.substring(message.indexOf('+') + 1).toInt());
            }

            if (counter++ % 50 == 0) {
                bluetooth->sendMessage("TEMP+" + String(context->getTemperature()));
            }
    }
}
