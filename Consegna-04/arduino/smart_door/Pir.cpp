#include "Pir.h"

Pir::Pir(int pin) {
    this->pin = pin;
    pinMode(this->pin, INPUT);
}

bool Pir::getPresence() {
    return digitalRead(pin) == HIGH;
}

void Pir::reset() {}
