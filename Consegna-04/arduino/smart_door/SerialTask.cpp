/**
 * Messaggi che vengono scritti nella seriale:
 * END                    : Inviato quando è stato inviato il comando di chiusura
 * OPEN                   : Inviato quando è stata rilevata la presenza con pir
 * TIMEOUT                : Iviato quando NON è stata rilevata alcuna presenza
 * LOGIN+username:passwd  : Credenziali inviate per fare il controllo del login
 */

#include "SerialTask.h"

SerialTask::SerialTask() {
    context = Context::getInstance();
}

void SerialTask::init(int period) {
    Task::init(period);
    this->period = period;
    state = CLOSED;
}

void SerialTask::tick() {
    String serialCommand = "";

    switch (state) {
        case CLOSED:
            if (context->getGlobal() == CHECK_LOGIN) {
                state = LOGIN;
                Log::info("SerialTask: CLOSED -> LOGIN");
            }

            break;
        case LOGIN:
            static bool loginEntryFlag = false;

            if (!loginEntryFlag) {
                sendMessage("LOGIN+" + context->getUsername() + SEPARATOR + context->getPassword());
                loginEntryFlag = true;
                return;
            }

            while (Serial.available()) {
                serialCommand += (char) Serial.read();
            }

            if (serialCommand == "LOGIN_OK\n") {
                context->setGlobal(ACCESS_GRANTED);
                state = OPENING;
                loginEntryFlag = false;
                Log::info("SerialTask: LOGIN -> OPENING");
            } else if (serialCommand == "LOGIN_KO\n") {
                context->setGlobal(ACCESS_DENIED);
                state = CLOSED;
                loginEntryFlag = false;
                Log::info("SerialTask: LOGIN -> CLOSED");
            }

            break;
        case OPENING:
            if (context->getGlobal() == PRESENCE_UNDETECTED) {
                sendMessage("TIMEOUT");
                state = CLOSED;
                Log::info("SerialTask: OPENING -> CLOSED");
            } else if (context->getGlobal() == PRESENCE_DETECTED) {
                sendMessage("OPEN");
                state = OPEN;
                Log::info("SerialTask: OPENING -> OPEN");
            }

            break;
        case OPEN:
            if (context->getGlobal() == COMMAND_CLOSE) {
                sendMessage("END");
                state = CLOSED;
                Log::info("SerialTask: OPEN -> CLOSED");
            }
            break;
    }
}

void SerialTask::sendMessage(String msg) {
    Serial.println(msg);
    Serial.flush();
}
