#ifndef __TEMPERATURE__
#define __TEMPERATURE__

#include "Arduino.h"
#include <math.h>

/**
 * This class is built for the sensor LM35DZ.
 */
class Temperature {
    public:
        Temperature(int pin);
        int getTemperature();
    private:
        int pin;
};

#endif
