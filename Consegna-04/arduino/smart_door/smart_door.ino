/****************************************************************
 *                                                              *
 *   smart_door.ino                                             *
 *                                                              *
 *   Written in 2017 by                                         *
 *   Nicolas Barilari <nicolas.barilari(at)studio.unibo.it>     *
 *   Emiliano Ciavatta <emiliano.ciavatta(at)studio.unibo.it>   *
 *   Simone Golinucci <simone.golinucci(at)studio.unibo.it>     *
 *                                                              *
 ****************************************************************/

#include "Scheduler.h"
#include "BluetoothTask.h"
#include "Log.h"
#include "MainTask.h"
#include "SerialTask.h"
#include "Task.h"
#include "UpdateTask.h"

Scheduler scheduler;

void setup() {
    Serial.begin(9600);
    scheduler.init(100);
    Log::enableDebug(false);

    /* MainTask, BluetoothTask and SerialTask must have the same schedule time
     * and must be executed in the order indicated above */
    Task* mainTask = new MainTask();
    mainTask->init(100);
    scheduler.addTask(mainTask);

    Task* bluetoothTask = new BluetoothTask();
    bluetoothTask->init(100);
    scheduler.addTask(bluetoothTask);

    Task* serialTask = new SerialTask();
    serialTask->init(100);
    scheduler.addTask(serialTask);

    /* This task is independent respect the others */
    Task* updateTask = new UpdateTask();
    updateTask->init(10000);
    scheduler.addTask(updateTask);
}

void loop() {
    scheduler.schedule();
}
