#include "UpdateTask.h"

UpdateTask::UpdateTask() {
    context = Context::getInstance();
}

void UpdateTask::init(int period) {
    Task::init(period);
    this->period = period;
}

void UpdateTask::tick() {
    Serial.println("VALUES+" + String(context->getTemperature()) + SEPARATOR + String(context->getLedIntensity()));
    Serial.flush();
}
