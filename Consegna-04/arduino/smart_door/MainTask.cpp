#include "MainTask.h"

MainTask::MainTask() {
    context = Context::getInstance();
    ledValue = new Led(6);
    ledOn = new Led(13);
    servoMotor.attach(9);
    pir = new Pir(2);
    prox = new Prox(5, 4);
    temp = new Temperature(A0);
    btnExit = new Button(7);
}

void MainTask::init(int period) {
    Task::init(period);
    this->period = period;
    state = CLOSED;
}

void MainTask::tick() {
    static unsigned int counter;

    switch (state) {
        case CLOSED:
            static bool closedEntryFlag = false;

            if (!closedEntryFlag) {
                ledValue->switchOff();
                ledOn->switchOn();
                servoMotor.write(1);
                context->setGlobal(NOTHING);
                closedEntryFlag = true;
            } else if (prox->getDistance() <= MIN_DIST) {
                closedEntryFlag = false;
                state = WAIT;
                Log::info("MainTask: CLOSED -> WAIT");
            }

            break;
        case WAIT:
            static bool waitEntryFlag = false;

            if (!waitEntryFlag) {
                counter = 0;
                waitEntryFlag = true;
            } else if (prox->getDistance() > MIN_DIST) {
                waitEntryFlag = false;
                state = CLOSED;
                Log::info("MainTask: WAIT -> CLOSED");
            } else if (period * counter >= MIN_SEC) {
                context->setGlobal(SAY_HELLO);
                waitEntryFlag = false;
                state = LOGIN;
                Log::info("MainTask: WAIT -> LOGIN");
            }

            counter++;

            break;
        case LOGIN:
            if (context->getGlobal() == ACCESS_GRANTED) {
                state = OPENING;
                Log::info("MainTask: LOGIN -> OPENING");
            } else if (context->getGlobal() == MAX_ATTEMPTS_REACHED) {
                state = CLOSED;
                Log::info("MainTask: LOGIN -> CLOSED");
            }

            break;
        case OPENING:
            static bool openingEntryFlag = false;

            if (!openingEntryFlag) {
                servoMotor.write(180);
                counter = 0;
                openingEntryFlag = true;
            } else if (pir->getPresence()) {
                context->setGlobal(PRESENCE_DETECTED);
                ledValue->switchOff();
                context->setLedIntensity(0);
                state = OPEN;
                openingEntryFlag = false;
                Log::info("MainTask: OPENING -> OPEN");
            } else if (period * counter >= MAX_DELAY) {
                context->setGlobal(PRESENCE_UNDETECTED);
                state = CLOSED;
                openingEntryFlag = false;
                Log::info("MainTask: OPENING -> CLOSED");
            }

            counter++;

            break;
        case OPEN:
            ledValue->setIntensity((double) context->getLedIntensity() / 100.0);

            if (btnExit->isPressed()) {
                context->setGlobal(COMMAND_CLOSE);
            } else if (context->getGlobal() == COMMAND_CLOSE) {
                state = CLOSED;
                Log::info("MainTask: OPEN -> CLOSED");
            }
    }

    context->setTemperature(temp->getTemperature());
}
