#include "Context.h"

Context *Context::instance = new Context();

Context::Context() {
    this->global = NOTHING;
    this->username = "";
    this->password = "";
    this->temperature = 0;
    this->ledIntensity = 0;
}

Context *Context::getInstance() {
    return Context::instance;
}

int Context::getGlobal() {
    return this->global;
}

void Context::setGlobal(int global) {
    this->global = global;
}

int Context::getTemperature() {
    return this->temperature;
}

void Context::setTemperature(int temperature) {
    this->temperature = temperature;
}

void Context::setLedIntensity(int intensity) {
    this->ledIntensity = intensity;
}

int Context::getLedIntensity() {
    return this->ledIntensity;
}

void Context::setUsername(String username) {
    this->username = username;
}

String Context::getUsername() {
    return this->username;
}

void Context::setPassword(String password) {
    this->password = password;
}

String Context::getPassword() {
    return this->password;
}
