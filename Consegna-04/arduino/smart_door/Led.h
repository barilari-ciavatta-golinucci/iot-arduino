#ifndef __LED__
#define __LED__

#include "Arduino.h"

class Led {
    public:
        Led(unsigned int pin);
        void switchOn();
        void switchOff();
        void toggle();
        void incrementFade(unsigned int fadeAmount);
        void decrementFade(unsigned int fadeAmount);
        void setIntensity(double intensity);
        bool isMinIntensityReached();
        bool isMaxIntensityReached();
        void destroy();
    private:
        unsigned int pin;
        bool isSwitchedOn;
        int currIntensity;
        void updateIntensity();
};

#endif
