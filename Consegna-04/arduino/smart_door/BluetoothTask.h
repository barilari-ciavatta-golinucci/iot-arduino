#ifndef __BLUETOOTHTASK__
#define __BLUETOOTHTASK__

#include "Arduino.h"
#include "Bluetooth.h"
#include "Context.h"
#include "Log.h"
#include "Task.h"

class BluetoothTask: public Task {
    enum State {WORKING, LOGIN, OPEN};

    public:
        BluetoothTask();
        void init(int period);
        void tick();

    private:
        Context *context;
        Bluetooth *bluetooth;
        State state;
        unsigned int period;
};

#endif
