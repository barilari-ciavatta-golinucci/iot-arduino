#ifndef __BUTTON__
#define __BUTTON__

#include "Arduino.h"

class Button { 
    public:
        Button(unsigned int pin);
        bool isPressed();
        void destroy();
    private:
        int pin;
};

#endif
