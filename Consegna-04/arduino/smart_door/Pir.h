#ifndef __PHYSICAL_PIR__
#define __PHYSICAL_PIR__

#include "Arduino.h"

class Pir{
    public:
        Pir(int pin);
        bool getPresence();
        void reset();
    private:
        unsigned int pin;
};

#endif
