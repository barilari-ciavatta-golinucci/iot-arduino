#ifndef __TIMER__
#define __TIMER__

#include "Arduino.h"
#include "FlexiTimer2.h"

class Timer {

public:
    Timer();
    void setupPeriod(int period);  // period in ms
    void waitForNextTick();
};

#endif
