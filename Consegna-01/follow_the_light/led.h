#ifndef __LED__
#define __LED__

/**
 * Causes the switch on of the led which corresponds to the specifies pin number.
 */
void led_switch_on(unsigned int pin);

/**
 * Causes the switch off of the led which corresponds to the specifies pin number.
 */
void led_switch_off(unsigned int pin);

/**
 * Causes the fade of the led which corresponds to the specifies pin number.
 */
void led_fade(unsigned int pin);   //do not use on pin 9 and 10

/**
 * Causes the blink of the led which corresponds to the specifies pin number.
 */
void led_blink(unsigned int pin, unsigned int interval);

#endif
