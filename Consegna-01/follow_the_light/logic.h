#ifndef __LOGIC__
#define __LOGIC__

/**
 * Enumeration that lists the types of game states.
 */
typedef enum {INIT, VIEW, PLAY, GAME_OVER} state;

/**
 * Inilializes the game with the total number of leds present (without considering the flash led).
 */
void game_init(unsigned int number_of_leds);

/**
 * Starts the game with the specified difficulty.
 */
void game_start(unsigned int difficulty);

/**
 * Returns the element previously added to the sequence.
 */
byte get_sequence_element();

/**
 * Returns the player's score possibly adding a bonus for the difficulty.
 */
unsigned int get_score();

/**
 * Returns the current game state.
 */
state get_current_state();

/**
 * Check whether the player's choice is correct or not and acts accordingly.
 */
void user_choise(byte choice);

/**
 * Ends the game.
 */
void game_over();

/**
 * Prints the entire sequence through the Serial.
 */
void print_sequence();

/**
 * Returns the length of the sequence.
 */
unsigned int get_sequence_length();

#endif
