#include <Arduino.h>
#include "TimerOne.h"
#include "led.h"

#define MAX_PIN 14   //the maximum number of available pins

#define OFF 0    //led switch on state
#define ON 1     //led switch off state
#define BLINK 2  //led blink state
#define FADE 3   //led fade state

//file visibility
static byte status[MAX_PIN] = { OFF };   //leds' states
static unsigned int current_led_blink_fade = -1;   //specifies the pin number of the led blinking or fading, -1 if no one currently exist

static int brightness;    //the number which specified the led brightness
static int fade_amount;   //the amount by which the led light is increased or reduced progressively
static boolean is_switched_on = false;

static void led_stop_last_blink_fade()
{
    if (current_led_blink_fade > -1)
    {
        status[current_led_blink_fade] = OFF;
        digitalWrite(current_led_blink_fade, LOW);  //switch off the led
        current_led_blink_fade = -1;
        Timer1.stop();
    }
}

static void led_fade_callback()
{
    analogWrite(current_led_blink_fade, brightness);
    brightness = brightness + fade_amount;

    if (brightness <= 0 || brightness >= 255)   //<= and >= is for more safety
    {
        fade_amount = -fade_amount;
    }
}

static void led_blink_callback()
{
    digitalWrite(current_led_blink_fade, !is_switched_on);
    is_switched_on = !is_switched_on;
}

void led_switch_on(unsigned int pin)
{
    if (status[pin] == BLINK || status[pin] == FADE)
    {
        led_stop_last_blink_fade();
    }
    status[pin] = ON;
    pinMode(pin, OUTPUT);
    digitalWrite(pin, HIGH);
}

void led_switch_off(unsigned int pin)
{
    if (status[pin] == BLINK || status[pin] == FADE)
    {
        led_stop_last_blink_fade();
    }
    status[pin] = OFF;
    digitalWrite(pin, LOW);
}

void led_fade(unsigned int pin)
{
    led_stop_last_blink_fade();
    status[pin] = FADE;
    pinMode(pin, OUTPUT);
    brightness = 0;
    fade_amount = 5;
    current_led_blink_fade = pin;
    Timer1.initialize(30UL * 1000UL);   //microseconds
    Timer1.attachInterrupt(led_fade_callback);
}

void led_blink(unsigned int pin, unsigned int velocity)
{
    led_stop_last_blink_fade();
    status[pin] = BLINK;
    pinMode(pin, OUTPUT);
    is_switched_on = false;
    current_led_blink_fade = pin;
    Timer1.initialize(velocity * 1000UL);   //microseconds
    Timer1.attachInterrupt(led_blink_callback);
}

