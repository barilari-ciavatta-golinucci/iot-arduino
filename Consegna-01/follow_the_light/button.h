#ifndef __BUTTON__
#define __BUTTON__

/**
 * Calls the specified function when detects a click event on the button joined to the specified pin number.
 */
void button_attach_click_event(unsigned int pin, void (*handler)(unsigned int));

/**
 * Detaches click events on the button joined to the specified pin number.
 */
void button_detach_click_event(unsigned int pin);

#endif
