#include <Arduino.h>
#include "potentiometer.h"

unsigned int potentiometer_get_value_in_range(unsigned int pin_number, unsigned int low_bound, unsigned int high_bound)
{
    return map(analogRead(pin_number), 0, 1023, low_bound, high_bound);
}
