#include <Arduino.h>
#include "logic.h"

#define SEQUENCE_MAX_LENGTH 256   //the maximum sequence length allowed to guess by the player
#define UNUSED_PIN A5             //unused (unconnected) pin

//file visibility
static state game_state;                   //the current state of the game
static byte sequence[SEQUENCE_MAX_LENGTH]; //the sequence of lights given to the player to guess
static unsigned int sequence_length;       //the current sequence length
static unsigned int progress;              //the number used as an index for scrolling through the sequence
static unsigned int score;                 //the current player's score
static unsigned int random_max;            //the upper bound number to use for generating random values
static unsigned int score_multiplier;      //current value of the multiplier applied to player's score, it represents the game difficulty

static void extract_next_element();

void game_init(unsigned int number_of_leds)
{
    game_state = INIT;
    sequence_length = 0;
    progress = 0;
    score = 0;
    random_max = number_of_leds;
    randomSeed(analogRead(A5));  //analogRead on an unused (unconnected) pin in order to initialize the random number generator with a fairly random input
}

void game_start(unsigned int difficulty)
{
    score_multiplier = difficulty;
    game_state = VIEW;
    extract_next_element();
}

byte get_sequence_element()
{
    byte element = sequence[progress];
    if (++progress >= sequence_length)  //if the sequence has been reproduced entirely
    {
        game_state = PLAY;
        progress = 0;
    }

    return element;
}

static void extract_next_element()
{
    byte extracted = random(random_max);
    sequence[sequence_length++] = extracted;

#ifdef DEBUG
    Serial.print("Last extracted led: ");
    Serial.println(extracted);
    print_sequence();
#endif
}

unsigned int get_score()
{
    return score * score_multiplier;
}

state get_current_state()
{
    return game_state;
}

void user_choise(byte choice)
{
    if (sequence[progress] == choice)   //if the player guesses an element of the sequence
    {
        progress++;
    }
    else
    {
        game_state = GAME_OVER;
    }

    if (progress >= sequence_length)   //if the player guesses the entire sequence
    {
        progress = 0;
        score += sequence_length;

        if (SEQUENCE_MAX_LENGTH <= sequence_length)   //if the player reaches the max limit length of the sequence
        {
            game_state = GAME_OVER;
        }
        else
        {
            game_state = VIEW;
            extract_next_element();
        }
    }

#ifdef DEBUG
    Serial.print("User choice: ");
    Serial.println(choice);
#endif
}

void game_over()
{
    game_state = GAME_OVER;
}

void print_sequence()
{
    Serial.print("Full sequence: ");
    for (int i = 0; i < sequence_length; i++)
    {
        Serial.print(sequence[i]);
        Serial.print(" ");
    }
    Serial.println();
}

unsigned int get_sequence_length()
{
    return sequence_length;
}
