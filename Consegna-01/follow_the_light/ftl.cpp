#include "ftl.h"

#define SEQUENCE_MAX_LENGTH 256

state game_state;
byte sequence[SEQUENCE_MAX_LENGTH];
int sequence_length;
int progress;
int points;
int ran_max;

void extract_next_element();

void game_init(int num_led)
{
    game_state = INIT;
    sequence_length = 0;
    progress = 0;
    points = 0;
    ran_max = num_led;
}

void game_start()
{
    game_state = VIEW;
    extract_next_element();
}

byte get_sequence_element(){
    int i=0;
    for(i=0; i<=sequence_length; i++){
        digitalWrite(sequence[i]+9,HIGH);
        delay(500);
        digitalWrite(sequence[i]+9,LOW);
        delay(500);
    }   
    game_state = PLAY;
    progress = 0; 
    /*
    int element = sequence[progress];
    if (++progress > sequence_length)
    {
        game_state = PLAY;
        progress = 0;
    }

    return element;*/
}

void extract_next_element()
{
    sequence[sequence_length++] = random(0, ran_max);
}

int get_points()
{
    return points;
}

state get_current_state()
{
    return game_state;
}

void user_choise(byte choise)
{  
    if (sequence[progress] == choise)
    {
        progress++;
    }
    else
    {
        game_state = GAME_OVER;
    }

    if (progress > sequence_length)
    {
        game_state = VIEW;
        progress = 0;
        points += sequence_length;
        extract_next_element();
    }
}

void game_over()
{
    Timer1.detachInterrupt();
    game_state = GAME_OVER;
}
