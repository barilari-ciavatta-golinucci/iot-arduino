#include <Arduino.h>
#include "EnableInterrupt.h"
#include "button.h"

#define MAX_PIN 14   //the maximum number of available pins
#define BOUNCING_DELAY 250   //the time (in milliseconds) in which disable buttons in order to prevent bouncing events

//file visibility
volatile static unsigned long times[MAX_PIN] = {0};  //array of values that represent the moments in which buttons are pressed.
                                                     //They're used for controlling buttons' bouncing.
static void (*handlers[MAX_PIN])(unsigned int);  //function pointers array

static void button_click(unsigned int pin)
{
    if (times[pin] + BOUNCING_DELAY < millis()) {  //prevent buttons' bouncing
        times[pin] = millis();
        handlers[pin](pin);
    }
}

//in order to handle every possible button
static void button_0() { button_click(0); }
static void button_1() { button_click(1); }
static void button_2() { button_click(2); }
static void button_3() { button_click(3); }
static void button_4() { button_click(4); }
static void button_5() { button_click(5); }
static void button_6() { button_click(6); }
static void button_7() { button_click(7); }
static void button_8() { button_click(8); }
static void button_9() { button_click(9); }
static void button_10() { button_click(10); }
static void button_11() { button_click(11); }
static void button_12() { button_click(12); }
static void button_13() { button_click(13); }

void button_attach_click_event(unsigned int pin, void (*handler)(unsigned int))
{
    pinMode(pin, INPUT);        //set the pin to input
    digitalWrite(pin, HIGH);    //use the internal pullup resistor
    times[pin] = 0;
    handlers[pin] = handler;

    switch (pin)
    {
        case 0:
            enableInterrupt(pin, button_0, RISING);
            break;
        case 1:
            enableInterrupt(pin, button_1, RISING);
            break;
        case 2:
            enableInterrupt(pin, button_2, RISING);
            break;
        case 3:
            enableInterrupt(pin, button_3, RISING);
            break;
        case 4:
            enableInterrupt(pin, button_4, RISING);
            break;
        case 5:
            enableInterrupt(pin, button_5, RISING);
            break;
        case 6:
            enableInterrupt(pin, button_6, RISING);
            break;
        case 7:
            enableInterrupt(pin, button_7, RISING);
            break;
        case 8:
            enableInterrupt(pin, button_8, RISING);
            break;
        case 9:
            enableInterrupt(pin, button_9, RISING);
            break;
        case 10:
            enableInterrupt(pin, button_10, RISING);
            break;
        case 11:
            enableInterrupt(pin, button_11, RISING);
            break;
        case 12:
            enableInterrupt(pin, button_12, RISING);
            break;
        case 13:
            enableInterrupt(pin, button_13, RISING);
            break;
        default:
            break;
    }
}

void button_detach_click_event(unsigned int pin)
{
    digitalWrite(pin, LOW);    //use the internal pullup resistor
    disableInterrupt(pin);
}
