/* * * * * * * * * * * * * * * * * * *
 *  Consegna #1 - Follow-the-Light!  *
 *                                   *
 *  Componenti gruppo:               *
 *  - Nicolas Barilari               *
 *  - Emiliano Ciavatta              *
 *  - Simone Golinucci               *
 * * * * * * * * * * * * * * * * * * */

#include "TimerOne.h"
#include "button.h"
#include "led.h"
#include "potentiometer.h"
#include "logic.h"

#define LED_1 9      //output pin number corresponding to the led n.1
#define LED_2 10     //output pin number corresponding to the led n.2
#define LED_3 11     //output pin number corresponding to the led n.3
#define LED_FLASH 6  //output pin number corresponding to the flash led

#define BUTTON_1 2   //input pin number corresponding to the button n.1
#define BUTTON_2 3   //input pin number corresponding to the button n.2
#define BUTTON_3 4   //input pin number corresponding to the button n.3

#define MIN_LED_VELOCITY 100     //minimum time (in milliseconds) in which a led can be switched on
#define MAX_LED_VELOCITY 1000    //maximum time (in milliseconds) in which a led can be switched on
#define MIN_PLAY_VELOCITY 800    //minimum time (in milliseconds) given to the player to guess a single light of the sequence
#define MAX_PLAY_VELOCITY 3000   //maximum time (in milliseconds) given to the player to guess a single light of the sequence
#define GAME_OVER_INTERVAL 4000  //time (in milliseconds) in which the led flash remains switched on, signaling the end of the game
#define MIN_SCORE_MULTIPLIER 1   //multiplier applied to player's score in the simplest game mode
#define MAX_SCORE_MULTIPLIER 10  //multiplier applied to player's score in the most difficult game mode

//file visibility
static unsigned int led_velocity;      //current time (in milliseconds) in which a led can be switched on
static unsigned int play_velocity;     //current time (in milliseconds) given to the player to guess a single light of the sequence
static unsigned int score_multiplier;  //current value of the multiplier applied to player's score, it represents the game difficulty

static unsigned int current_led_blink = -1;   //specifies the pin number of the led blinking, -1 if no one currently exist

void setup()
{
    Serial.begin(9600);
    setup_game();
}

void loop() {}

static void setup_game()
{
    destroy_timer();
    game_init(3);
    led_fade(LED_FLASH);
    Serial.println("Welcome to Follow the Light!");
    button_attach_click_event(BUTTON_1, button_start_handler);
}

static void button_start_handler(unsigned int pin)
{
    button_detach_click_event(BUTTON_1);

    led_velocity = potentiometer_get_value_in_range(A0, MIN_LED_VELOCITY, MAX_LED_VELOCITY);
    play_velocity = potentiometer_get_value_in_range(A0, MIN_PLAY_VELOCITY, MAX_PLAY_VELOCITY);
    score_multiplier = MAX_SCORE_MULTIPLIER - potentiometer_get_value_in_range(A0, MIN_SCORE_MULTIPLIER, MAX_SCORE_MULTIPLIER);

    led_switch_off(LED_FLASH);
    game_start(score_multiplier);
    start_view_state();

    Serial.print("Ready! - Difficulty: ");
    Serial.println(score_multiplier);
}

static void start_view_state()
{
    set_timer(blink_led_callback, led_velocity);
}

static void blink_led_callback()
{
    if (current_led_blink == -1)  //if no led is blinking
    {
        current_led_blink = get_sequence_element() + LED_1;
        led_switch_on(current_led_blink);
        return;
    }
    else
    {
        led_switch_off(current_led_blink);
        current_led_blink = -1;
    }

    if (get_current_state() == PLAY)
    {
        start_play_state();
    }
}

static void start_play_state()
{
    set_timer(time_expired_callback, get_sequence_length() * play_velocity);
    attach_click_event();  //from now on the player can guess the sequence clicking buttons
}

static void time_expired_callback()
{
    Serial.println("Time expired!");
    game_over();
    start_game_over_state();
}

static void button_click_handler(unsigned int pin)
{
    user_choise(pin - BUTTON_1);

    if (get_current_state() == VIEW)
    {
        detach_click_event();
        start_view_state();
    }
    else if (get_current_state() == GAME_OVER)
    {
        Serial.println("Wrong button!");
        start_game_over_state();
    }
}

static void start_game_over_state()
{
    detach_click_event();
    Serial.print("Game Over - Score: ");
    Serial.println(get_score());
    led_switch_on(LED_FLASH);
    set_timer(setup_game, GAME_OVER_INTERVAL);
}

static void attach_click_event()
{
    button_attach_click_event(BUTTON_1, button_click_handler);
    button_attach_click_event(BUTTON_2, button_click_handler);
    button_attach_click_event(BUTTON_3, button_click_handler);
}

static void detach_click_event()
{
    button_detach_click_event(BUTTON_1);
    button_detach_click_event(BUTTON_2);
    button_detach_click_event(BUTTON_3);
}

static void set_timer(void (*callback)(), unsigned int velocity)
{
    destroy_timer();
    Timer1.initialize((unsigned long)velocity * 1000L);   //microseconds
    Timer1.attachInterrupt(callback);
}

static void destroy_timer()
{
    Timer1.stop();
    Timer1.detachInterrupt();
}
