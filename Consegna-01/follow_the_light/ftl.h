#include <Arduino.h>
#include <TimerOne.h>

typedef enum {INIT, VIEW, PLAY, GAME_OVER} state;

void game_init(int num_led);
void game_start();
byte get_sequence_element();
int get_points();
state get_current_state();
void user_choise(byte choise);
void game_over();
