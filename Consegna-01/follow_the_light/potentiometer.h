#ifndef __POTENTIOMETER__
#define __POTENTIOMETER__

/**
 * Accepts a pin number and two integer representing two bounds.
 * Re-maps the value read from the specified pin from on bound (low_bound) to the other (high_bound).
 */
unsigned int potentiometer_get_value_in_range(unsigned int pin_number, unsigned int low_bound, unsigned int high_bound);

#endif
